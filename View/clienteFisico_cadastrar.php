<?php
    require_once ("../Controller/crudClienteFisico.php");   
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">

        <link href="css/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/cavalaro.css" type="text/css" media="all" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="js/bootstrap-sweetalert-master/lib/sweet-alert.css">

        <title>Cadastrar Cliente Físico</title>
    </head>
    <body class="bgMain">
        <header>
          <?php include 'header.php';?>
        </header>

          <div class="container">
          <a href="clienteFisico_listar.php?p=0">
               <i class="fa fa-arrow-circle-left fa-3x pull-left" aria-hidden="true" title="Voltar"></i>
              </a>
              <div class="row">
            
                  <div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3 bgForm">


                      <div class="panel-heading">
                          <h3 class="panel-title text-center" style="font-family:Open Sans; color:white; font-size: 25px;">Dados do Segurado</h3>
                      </div>
                      <div class="panel-body">
                          <form role="form" method="post" action="../Controller/crudClienteFisico.php" id="form" data-parsley-validate>
                              <div class="form-group">
      			    				<input type="name" name="nome" class="form-control input-sm" placeholder="NOME COMPLETO" autofocus required>
                              </div>
                              <div class="row">
      			    				<div class="col-xs-6 col-sm-6 col-md-6">
      			    					<div class="form-group">
                                <input type="text" name="cpf" class="form-control input-sm cpf" placeholder="CPF" data-parsley-cpf data-parsley-trigger="blur" required>
      			    					</div>
      			    				</div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="rg" class="form-control input-sm rg" placeholder="RG" required>
                                </div>
                            </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6" >
                                      <div class="form-group">
                                          <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" name="datanasc" class="form-control input-sm" placeholder="DATA DE NASCIMENTO" >
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
      			    						<input type="number" name="idade" class="form-control input-sm" placeholder="IDADE" data-parsley-trigger="blur">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <select class="form-control" name="sexo" required>
                                              <option value="" hidden selected disabled>SEXO</option>
                                              <option value="Feminino">Feminino</option>
                                              <option value="Masculino">Masculino</option>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
      			    						<input type="text" name="profissao" class="form-control input-sm" placeholder="PROFISSÃO">
                                      </div>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <input type="text" name="endereco" class="form-control input-sm" placeholder="ENDEREÇO">
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <select class="form-control" name="estadocivil">
                                              <option value="" hidden selected disabled>ESTADO CIVÍL</option>
                                              <option value="Casado" >Casado</option>
                                              <option value="Solteiro ">Solteiro</option>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
      			    						<input type="text" name="cnh" class="form-control input-sm" placeholder="CNH">
                                      </div>
                                  </div>
                              </div>
                               <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" name="cep" placeholder="CEP" class="form-control input-sm cep">
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" name="cidade"  placeholder="CIDADE" class="form-control input-sm">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" name="bairro" placeholder="BAIRRO" class="form-control input-sm">
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" name="uf"  placeholder="UF" class="form-control input-sm">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" name="telefone" placeholder="TELEFONE" class="form-control input-sm phone">
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="email" name="email" placeholder="EMAIL" class="form-control input-sm" data-parsley-type="email">
                                      </div>
                                  </div>
                              </div>
                              
                              <div class="form-group text-center">
                                  <input type="submit" value="Cadastrar" name="Cadastrar" class="btn btAlterarCadastrar">
                              </div>
                          </form>
                      </div>
                  </div>
          </div>
        </div>
     
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/Parsley.js-2.3.5/dist/parsley.min.js"></script>
        <script type="text/javascript" src="js/parsleyjs-validators-master/dist/cpf.js"></script>
        <script type="text/javascript" src="js/parsleyjs-validators-master/dist/i18n/pt-br.js"></script>
        <script type="text/javascript" src="js/Parsley.js-2.3.5/dist/i18n/pt-br.js"></script>
        <script type="text/javascript" src="js/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js"> </script>
        <script type="text/javascript" src="js/bootstrap-sweetalert-master/lib/sweet-alert.js"></script>

        
        
        <?php 

          if(isset($_SESSION['mensagem'])){ ?>
                       <script>
                           $(document).ready(function() {
                                swal("Sucesso", "Usuário cadastrado com sucesso!", "success")
                           })
                        </script>
              
                      <?php unset($_SESSION['mensagem']);
                    }
          ?>
        
        <script> $(document).ready(function(){
            $('.date').mask('11/11/1111');
            $('.time').mask('00:00:00');
            $('.date_time').mask('00/00/0000 00:00:00');
            $('.cep').mask('00000-000');
            $('.phone').mask('0000-0000');
            $('.phone_with_ddd').mask('(00) 0000-0000');
            $('.phone_us').mask('(000) 000-0000');
            $('.mixed').mask('AAA 000-S0S');
            $('.cpf').mask('000.000.000-00', {reverse: true});
            $('.rg').mask('00.000.000-0');
            $('.money').mask('000.000.000.000.000,00', {reverse: true});
}); 
        </script>
        <!-- Tratando telefones de São Paulo -->
        <script> 
            var maskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
                options = {onKeyPress: function(val, e, field, options) {
                    field.mask(maskBehavior.apply({}, arguments), options);
    }
};
            $('.phone').mask(maskBehavior, options); 
        </script>
        
         <script type="text/javascript">
            $('#form').parsley();
        </script>

    </body>
</html>