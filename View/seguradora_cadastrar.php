<?php
    require_once ("../Controller/crudSeguradora.php");   
?> 

<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">

        <link href="css/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/cavalaro.css" type="text/css" media="all" rel="stylesheet">

        <title>Seguradora</title>
    </head>
    <body class="bgMain">        
        <header>
          <?php include 'header.php';?>
        </header>
        
        <div class="container">
         <a href="seguradora_listar.php?p=0">
            <i class="fa fa-arrow-circle-left fa-3x pull-left" aria-hidden="true" title="Voltar"></i>
         </a>
          <div class="row">
          	<div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3">
          		<div class="panel-heading painel-form">
			    		<h3 class="col-md-offset-3 panel-title" style="font-family:Open Sans; color:white; font-size: 25px;">Dados da Seguradora</h3>
			 		</div>
			 	<div class="panel-body painel-form">

  			 <form role="form" method="post" action="../View/seguradora_cadastrar.php" >

	    			<div class="form-group">
	    				<input type="text" name="nomeFantasia" placeholder="NOME FANTASIA" class="form-control input-sm" required="">
	    			</div>

					<div class="form-group">
            			<input type="text" name="razaoSocial" placeholder="RAZÃO SOCIAL" class="form-control input-sm" required="">
					</div>

					<div class="form-group">
            			<input type="text" name="representante" placeholder="REPRESENTANTE" class="form-control input-sm" required="">
					</div>

    				<div class="row">
    					<div class="col-xs-6 col-sm-6 col-md-6">
	    					<div class="form-group">
	    						<input type="text" name="cnpj" placeholder="CNPJ" class="form-control input-sm" required="">
	    					</div>
    					</div>

    					<div class="col-xs-6 col-sm-6 col-md-6">
	    					<div class="form-group">
	    						<input type="text" name="cep" placeholder="CEP" class="form-control input-sm" required="">
	    					</div>
    					</div>
    				</div>

                     <div class="form-group">
                            <input type="text" name="endereco"  placeholder="ENDEREÇO" class="form-control input-sm">
                      </div>

    				<div class="row">
    					<div class="col-xs-6 col-sm-6 col-md-6">
	    					<div class="form-group">
	    						<input type="text" name="cidade" placeholder="CIDADE" class="form-control input-sm" required="">
	    					</div>
    					</div>

    					<div class="col-xs-6 col-sm-6 col-md-6">
    						<div class="form-group">
    							<input type="text" name="website" placeholder="WEBSITE" class="form-control input-sm" required="">
    						</div>
    					</div>
    				</div>
    				 <div class="row">
    					<div class="col-xs-6 col-sm-6 col-md-6">
	    					<div class="form-group">
	    						<input type="text" name="bairro" placeholder="BAIRRO" class="form-control input-sm" required="">
	    					</div>
    					</div>

    					<div class="col-xs-6 col-sm-6 col-md-6">
    						<div class="form-group">
    							<input type="text" name="uf" placeholder="UF" class="form-control input-sm" required="">
    						</div>
    					</div>
    				</div>

    				<div class="row">
    					<div class="col-xs-6 col-sm-6 col-md-6">
	    					<div class="form-group">
	    						<input type="text" name="telefone" placeholder="TELEFONE" class="form-control input-sm" required="">
	    					</div>
    					</div>

    					<div class="col-xs-6 col-sm-6 col-md-6">
    						<div class="form-group">
    							<input type="text" name="email" placeholder="E-MAIL" class="form-control input-sm" required="">
    						</div>
    					</div>
                        <div class="col-md-offset-5 pdtop">
                            <input type="submit" name="btnSubmit" value="Cadastrar" class="btn btn-info btn-lg btAlterarCadastrar">
                        </div>
    				</div>
  			    </div>
  			  </form>
  			  </div>
  			</div>
  		</div>
      </div>  		
    </div>
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js"> </script>
        <script type="text/javascript" src="js/bootstrap-sweetalert-master/lib/sweet-alert.js"></script>
        <?php 

          if(isset($_SESSION['mensagem'])){ ?>
                       <script>
                           $(document).ready(function() {
                                swal("Sucesso", "Usuário cadastrado com sucesso!", "success")
                           })
                        </script>
              
                      <?php unset($_SESSION['mensagem']);
                    }
          ?>
    </body>
</html>