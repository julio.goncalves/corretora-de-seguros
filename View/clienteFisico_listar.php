<?php
$pagina = @$_GET["p"];
    require_once ("../Controller/crudClienteFisico.php");    

?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <script src="js/jquery-1.12.0.min.js"></script>    
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/bootstrap-sweetalert-master/lib/sweet-alert.js"></script>
        <link href="css/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/cavalaro.css" type="text/css" media="all" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="js/bootstrap-sweetalert-master/lib/sweet-alert.css">
        

        <title>Cliente Fisico</title>
    </head>
    <body class="bgMain">

      <header>
        <?php include 'header.php';?>
      </header>

          <div class="container">
            <div class="col-md-2" >
              <a href="clienteFisico_cadastrar.php" style="padding-left: 19px;">
                <button class="btn btnAlterar">
                  <em class="fa fa-plus fa-2x"></em>
                </button>
              </a>
            </div>
          <?php 

          if(isset($_SESSION['mensagem'])){ ?>
                       <script>
                           $(document).ready(function() {
                                swal("Sucesso", "Cliente excluido com sucesso!", "success")
                           })
                        </script>
              
                      <?php unset($_SESSION['mensagem']);
                    }
          ?>

          <div class="col-md-6">
            <form method="post" action="../View/clienteFisico_listar.php?p=0" name="frmLogin">
                <div class="col-md-4">
                    <input type="text" name="nome" placeholder="Nome do Cliente" class="form-control"/>
                </div>
                <div class="col-md-2">
                    <input type="submit" name="btnBuscar" value="Pesquisar" class="btn  btn-lg btn-primary btAlterarCadastrar" />
                </div>
                <div class="col-md-8">
                <?php
                    if (isset($_POST['btnBuscar'])) {
                ?>
              <h3>Resultados de pesquisa por: 
                <kbd>
                  <?php
                      $nomeCliente = $_POST['nome'];  
                      echo "$nomeCliente"; 
                    }
                  ?> 
                </kbd>
              </h3>
            </div>
          </form>
        </div>

          <div class="col-md-4">
              <a href="clienteFisico_Relatorio.php?p=0">
                  <input type="submit" name="btnGerarRelatorio" value="Relatorios" class="btn btn-info btn-lg btn-danger btExcliuir" />
              </a>
          </div>        
        </div>

          <div class="col-md-10 col-md-offset-1">
            <div class="panel-body">
              <table class="table table-striped table-bordered table-list">
                <thead>
                  <tr>
                   <th ></th>
                    <th>Nome</th>
                    <th>CPF</th>
                    <th>RG</th>
                  </tr>
              </thead>
              <tbody>

                  <?php

                  foreach($dados as $row){
                      $idCliente = $row['idClienteFisico'];
                      $nomeCliente = $row['nomeCliente'];
                      $rgCliente = $row['rgCliente'];
                      $cpfCliente = $row['cpfCliente'];
                  
                  ?>

                <tr>
                    <form action="clienteFisico_alterar.php?idCliente=<?php echo $idCliente ?>" method="post">
                        <td align="center">
                            <button class="btn btnAlterar" value="Alterar" name="Alterar"><em class="fa fa-pencil"></em></button>
                            <a  data-confirm="Tem certeza que deseja excluir o(a) cliente <?php echo $nomeCliente ?> ?" href="../Controller/crudClienteFisico.php?id=<?php echo $idCliente; ?>" class="btn btn-danger"><em class="fa fa-trash"></em> </a>
                        </td>
                        
                        <input name="idClienteFisico" value="<?php echo $idCliente ?>" class="hidden form-control">
                        <input name="nomeCliente" value="<?php echo $nomeCliente ?>" class="hidden form-control">
                        <input name="rgCliente" value="<?php echo $rgCliente ?>" class="hidden form-control">

                  <td><?php echo $nomeCliente ?> </td>
                  <td><?php echo $cpfCliente ?> </td>
                  <td><?php echo $rgCliente ?> </td>
                    </form>
                </tr>

                <?php } ?>
              </tbody>
        </table>
                                
       <div class="panel-footer">
                <div class="row">
                  <div class="col col-xs-4"></div>
                  <div class="col col-xs-8">
                    <ul class="pagination hidden-xs pull-right">
                     <?php 
              $limite = 10; 
              $totalPaginas = $qtdPag/$limite;

              for($c=0;$c<=$totalPaginas;$c++){
                echo '<li><a href="clienteFisico_listar.php?p='.$c.'">'.$c.'</a></li>';
          }

          ?>
                </ul>
                <ul class="pagination visible-xs pull-right">
                    <li><a href="#">«</a></li>
                    <li><a href="#">»</a></li>
                </ul>
              </div>
            </div>

            <?php 
            if (empty($dados)) { ?>
              <div>
                   <p class="text-center" style="color:#EEE; font-size:25px;">Nenhuma cliente cadastrada!</p>
              </div>   
           <?php } ?> 

          </div>  
     
      <script type="text/javascript">

        $(document).ready(function() {

          $('a[data-confirm]').click(function(ev) {
              var href = $(this).attr('href');

              if (!$('#dataConfirmModal').length) {
                  $('body').append('<div id="dataConfirmModal" class="modal fade" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3 id="dataConfirmLabel">Excluir Cliente</h3></div><div class="modal-body"></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button><a class="btn btn-primary" id="dataConfirmOK">OK</a></div></div></div></div>');
              } 
              $('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm'));
              $('#dataConfirmOK').attr('href', href);
               $('#dataConfirmModal').modal({show:true});
              return false;
            });
        });

        </script>        
    </body>
</html>