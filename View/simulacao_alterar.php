<?php
    $idSeguro = $_GET["id"];
    require_once ("../Controller/crudSimulacao.php");

    if($row['sexoCliente']== 'M'){
      $sexoCliente = 'Masculino';
    }else{
      $sexoCliente = 'Feminino';
    }
?>

</html>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <script src="js/jquery-1.12.0.min.js"></script>    
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/bootstrap-sweetalert-master/lib/sweet-alert.js"></script>
        <link href="css/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/cavalaro.css" type="text/css" media="all" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="js/bootstrap-sweetalert-master/lib/sweet-alert.css">
        
        <title>Cavalaro - Home</title>
    </head>
    <body class="bgMain">  
    <header>
      <?php include 'header.php';?>
    </header>
        
        <div class="container pdbot">
        <div class="row">
        <a href="simulacao_listar.php">
          <i class="fa fa-arrow-circle-left fa-3x pull-left" aria-hidden="true" title="Voltar"></i>
        </a>  
        </div>
              <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3 bgForm">
                      <div class="panel-heading">
                          <h3 class="panel-title text-center" style="font-family:Open Sans; color:white; font-size: 25px;">Dados do Segurado</h3>
                      </div>
                      <div class="panel-body">
                          <form role="form" method="post" action="" id="form" data-parsley-validate>
                              <div class="form-group">
                            <label for="exampleInputEmail1">Nome</label>
                            <input type="name" name="nome" class="form-control input-sm" placeholder="" value="<?php echo $row['nomeCliente'] ; ?>">
                          </div>

                          <div class="row">
                           <div class="col-xs-6 col-sm-6 col-md-6">
                              <div class="form-group">
                              <label for="exampleInputEmail1">CPF</label>
                                  <input type="text" name="cpf" class="form-control input-sm" data-parsley-cpf data-parsley-trigger="blur" data-parsley-required value="<?php echo $row['cpfCliente'] ; ?>">
                              </div>
                           </div>
                           
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                <label for="exampleInputEmail1">RG</label>
                                    <input type="text" name="rg" class="form-control input-sm rg" value="<?php echo $row['rgCliente'] ; ?>">
                                </div>
                            </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                      <label for="exampleInputEmail1">Data de Nascimento</label>
                                          <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" name="datanasc" class="form-control input-sm" value="<?php echo $row['dataNascimento'] ; ?>">
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                      <label for="exampleInputEmail1">Idade</label>
                            <input type="text" name="idade" class="form-control input-sm" value="<?php echo $row['idade'] ; ?>">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                      <label for="exampleInputEmail1">Sexo</label>
                                          <select class="form-control" name="sexo">
                                              <option value="<?php echo $row['sexoCliente']; ?>"  hidden><?php echo $sexoCliente; ?></option>
                                              <option value="F">Feminino</option>
                                              <option value="M">Masculino</option>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                      <label for="exampleInputEmail1">Profissão</label>
                                      <input type="text" name="profissao" class="form-control input-sm" value="<?php echo $row['profissao'] ; ?>">
                                      </div>
                                  </div>
                              </div>
                              <div class="form-group">
                              <label for="exampleInputEmail1">Endereço</label>
                                  <input type="text" name="endereco" class="form-control input-sm" value="<?php echo $row['enderecoPessoa'] ; ?>">
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                      <label for="exampleInputEmail1">Estado Civil</label>
                                          <select class="form-control" name="estadocivil">
                                              <option value="<?php echo $row['estadocivil']; ?>" hidden><?php echo $row['estadocivil']; ?></option>
                                              <option value="Casado" >Casado</option>
                                              <option value="Solteiro ">Solteiro</option>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                      <label for="exampleInputEmail1">CNH</label>
                            <input type="text" name="cnh" class="form-control input-sm" value="<?php echo $row['cnhPessoa'] ; ?>">
                                      </div>
                                  </div>
                              </div>
                               <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                      <label for="exampleInputEmail1">CEP</label>
                                          <input type="text" name="cep"  value="<?php echo $row['cepPessoa']; ?>" class="form-control input-sm">
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                      <label for="exampleInputEmail1">Cidade</label>
                                          <input type="text" name="cidade"  value="<?php echo $row['cidadePessoa']; ?>" class="form-control input-sm">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                      <label for="exampleInputEmail1">Bairro</label>
                                          <input type="text" name="bairro" value="<?php echo $row['bairroPessoa']; ?>" class="form-control input-sm">
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                      <label for="exampleInputEmail1">UF</label>
                                          
                                          <select name="uf" class="form-control">
                                          <option value="<?php echo $row['ufPessoa']; ?>" hidden><?php echo $row['ufPessoa']; ?></option>
                                          <option value="AC">AC</option>
                                          <option value="AL">AL</option>
                                          <option value="AM">AM</option>
                                          <option value="AP">AP</option>
                                          <option value="BA">BA</option>
                                          <option value="CE">CE</option>
                                          <option value="DF">DF</option>
                                          <option value="ES">ES</option>
                                          <option value="GO">GO</option>
                                          <option value="MA">MA</option>
                                          <option value="MG">MG</option>
                                          <option value="MS">MS</option>
                                          <option value="MT">MT</option>
                                          <option value="PA">PA</option>
                                          <option value="PB">PB</option>
                                          <option value="PE">PE</option>
                                          <option value="PI">PI</option>
                                          <option value="PR">PR</option>
                                          <option value="RJ">RJ</option>
                                          <option value="RN">RN</option>
                                          <option value="RS">RS</option>
                                          <option value="RO">RO</option>
                                          <option value="RR">RR</option>
                                          <option value="SC">SC</option>
                                          <option value="SE">SE</option>
                                          <option value="SP">SP</option>
                                          <option value="TO">TO</option>
                                         </select>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                      <label for="exampleInputEmail1">Telefone</label>
                                          <input type="text" name="telefone" value="<?php echo $row['telefonePessoa']; ?>" class="form-control input-sm">
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                      <label for="exampleInputEmail1">E-mail</label>
                                          <input type="text" name="email" value="<?php echo $row['emailPessoa']; ?>" class="form-control input-sm">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                              <div class=" col-md-offset-2 col-xs-6 col-sm-6 col-md-6 text-center">
                                <label>Imagem da CNH</label>
                                <img class="imagesize img-responsive img-thumbnail" alt="imagem cnh" title="Imagem da CNH" src="<?php echo "../Controller/img/" . $row['imgurl'] ; ?>" >
                              </div>
                              </div>
 
                              </div>
                          </div>
                      </div>
                  </div>

            <div class="container pdbot">
              <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3 bgForm">
                      <div class="panel-heading">
                          <h3 class="panel-title text-center" style="font-family:Open Sans; color:white; font-size: 25px;">Dados do Automóvel</h3>
                      </div>
                      <div class="panel-body">                      
                              <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                    <label for="exampleInputFile" style="font-size: 15px;">Fabricante</label>
                                        <div class="form-group">
                                            <input type="text" name="fabricanteCarro" class="form-control input-sm" value="<?php echo $row['marca']; ?>" required>
                                        </div>
                                    </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                  <label for="exampleInputFile" style="font-size: 15px;">Modelo</label>
                                      <div class="form-group">
                                          <input type="text" name="modeloCarro" class="form-control input-sm" value="<?php echo $row['modelo']; ?>" required>
                                      </div>
                                  </div>
                              </div>
                               <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                  <label for="exampleInputFile" style="font-size: 15px;">Combustível</label>
                                      <div class="form-group">
                                          <select class="form-control" name="combustivelCarro" required>
                                              <option value="" hidden selected disabled><?php echo $row['Combustivel']; ?></option>
                                              <option value="Gasolina">Gasolina</option>
                                              <option value="Alcool">Alcool</option>
                                              <option value="Gasolina/Alcool">Gasol / Alcool</option>
                                              <option value="Kit Gas">Kit Gás</option>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                  <label for="exampleInputFile" style="font-size: 15px;">N° de Portas</label>
                                       <div class="form-group">
                                          <select class="form-control" name="nPortasCarro" required>
                                              <option value="" hidden selected disabled><?php echo $row['numeroPortas']; ?></option>
                                              <option value="2">2</option>
                                              <option value="3">3</option>
                                              <option value="4">4</option>
                                              <option value="5">5</option>
                                          </select>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                  <label for="exampleInputFile" style="font-size: 15px;">Ano do Veículo</label>
                                      <div class="form-group">
                                          <input type="text" name="anoCarro" class="form-control input-sm year" value="<?php echo $row['ano']; ?>" required>
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                  <label for="exampleInputFile" style="font-size: 15px;">Placa do Veiculo</label>
                                      <div class="form-group">
                                            <input type="text" name="placaCarro" class="form-control input-sm placa" value="<?php echo $row['placa']; ?>" data-parsley-trigger="blur">
                                      </div>
                                  </div>
                              </div>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="container pdbot">
                  <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3 bgForm">
                      <div class="panel-heading">
                          <h3 class="panel-title text-center" style="font-family:Open Sans; color:white; font-size: 25px;">Perfil do usuário</h3>
                      </div>
                      <div class="panel-body">
                              <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                    <label for="exampleInputFile" style="font-size: 15px;">Uso do Veículo</label>
                                        <div class="form-group">
                                          <select class="form-control" name="usoCarro" required>
                                              <option value="" hidden selected disabled><?php echo $row['usoVeiculo']; ?></option>
                                              <option value="Particular">Particular</option>
                                              <option value="Trabalho">Trabalho</option>
                                              <option value="Faculdade">Faculdade</option>
                                              <option value="Particular/Trabalho">Particular/Trabalho</option>
                                              <option value="Particular/Faculdade">Particular/Faculdade</option>


                                          </select>
                                      </div>
                                    </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                      <label for="exampleInputFile" style="font-size: 15px;">Possui Garagem?</label>
                                          <select class="form-control" name="garagemCarro" required>
                                              <option value="" hidden selected disabled><?php echo $row['garagem']; ?></option>
                                              <option value="Nao Possui">Não Possui</option>
                                              <option value="Trabalho">No Trabalho</option>
                                              <option value="Residencia">Na Residência</option>
                                              <option value="Faculdade">Na Faculdade</option>
                                          </select>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                <div class="col-md-12">
                                  <label>A garagem possui portão?</label> &nbsp;&nbsp;&nbsp;&nbsp;
                                  <?php if($row['possuiPortao'] == 'Sim'){ ?>
                                  <label class="radio-inline"><input type="radio" name="possuiPortao" checked="checked" value="Sim">Sim</label>
                                  <label class="radio-inline"><input type="radio" name="possuiPortao" value="nao">Não</label>
                                   <?php }else{ ?>
                                   <label class="radio-inline"><input type="radio" name="possuiPortao" value="Sim">Sim</label>
                                  <label class="radio-inline"><input type="radio" name="possuiPortao" checked="checked" value="nao">Não</label> <?php } ?>
                                </div>
                              </div>
                              <div class="row pdbot">
                                <div class="col-md-12">
                                  <label>Possui alarme, bloqueador e rastreador?</label> &nbsp;&nbsp;&nbsp;&nbsp;
                                  <?php if($row['seguranca'] == 'Sim'){ ?>
                                  <label class="radio-inline"><input type="radio" name="seguranca" checked="checked" value="Sim">Sim</label>
                                  <label class="radio-inline"><input type="radio" name="seguranca" value="nao">Não</label>
                                   <?php }else{ ?>
                                   <label class="radio-inline"><input type="radio" name="seguranca" value="Sim">Sim</label>
                                  <label class="radio-inline"><input type="radio" name="seguranca" checked="checked" value="nao">Não</label> <?php } ?>
                                </div>
                              </div>

                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6 col-md-offset-3" >
                                  <div class="form-group">
                                  <div class="input-group">
                                    <div class="input-group-addon">R$</div>
                                    <input type="text" name="valor" class="form-control input-sm" value="<?php echo $row['valor'];?>">
                                  </div>
                                </div>
                                  </div>
                              </div>
                              
                              <div class="form-group text-center ">
                                  <input type="submit" value="Cadastrar" name="Cadastrar" class="btn btAlterarCadastrar">
                              </div>     
                              </div>
                          </div>
                      </div>
                  </div>

                  <input type="date" name="data" value="<?php echo date('Y-m-d'); ?>" hidden>
                  <input type="text" name="idCliente" value="<?php echo  $idSeguro; ?>" hidden>
                </form>


        <script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/Parsley.js-2.3.5/dist/parsley.min.js"></script>
        <script type="text/javascript" src="js/parsleyjs-validators-master/dist/cpf.js"></script>
        <script type="text/javascript" src="js/parsleyjs-validators-master/dist/i18n/pt-br.js"></script>
        <script type="text/javascript" src="js/Parsley.js-2.3.5/dist/i18n/pt-br.js"></script>
        <script type="text/javascript" src="js/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js"> </script>
        <script type="text/javascript" src="js/bootstrap-sweetalert-master/lib/sweet-alert.js"></script>

        <?php
        if(isset($_SESSION['mensagem'])){
          ?>
          <script>
          $(document).ready(function() {
            swal("Sucesso", "Usuário cadastrado com sucesso!", "success")
          })
          </script>
           <?php unset($_SESSION['mensagem']);
        }

         ?>

        <script> $(document).ready(function(){
            $('.year').mask('0000');
            $('.time').mask('00:00:00');
            $('.date_time').mask('00/00/0000 00:00:00');
            $('.cep').mask('00000-000');
            $('.placa').mask('AAA-0000');
            $('.phone').mask('0000-0000');
            $('.phone_with_ddd').mask('(00) 0000-0000');
            $('.phone_us').mask('(000) 000-0000');
            $('.mixed').mask('AAA 000-S0S');
            $('.cpf').mask('000.000.000-00', {reverse: true});
            $('.rg').mask('00.000.000-0');
            $('.money').mask('000.000.000.000.000,00', {reverse: true});
}); 
        </script>
        <!-- Tratando telefones de São Paulo -->
        <script> 
            var maskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
                options = {onKeyPress: function(val, e, field, options) {
                    field.mask(maskBehavior.apply({}, arguments), options);
    }
};
            $('.phone').mask(maskBehavior, options); 
        </script>
        
         <script type="text/javascript">
            $('#form').parsley();
        </script>
     
    </body>    
</html>
