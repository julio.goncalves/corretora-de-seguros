<!DOCTYPE html>
<?php require_once ("../Controller/crudSeguro.php"); ?>
<html>
    <head>
        <meta charset="utf-8">
        
        <link href="css/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/cavalaro.css" type="text/css" media="all" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="js/bootstrap-sweetalert-master/lib/sweet-alert.css">

        
        <title>Cavalaro - Home</title>
    </head>
    <body class="bgMain">  
    <header>
      <?php include 'header.php';?>
    </header>
        
        <div class="container pdbot">
              <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3 bgForm">
                      <div class="panel-heading">
                          <h3 class="panel-title text-center" style="font-family:Open Sans; color:white; font-size: 25px;">Dados do Segurado</h3>
                      </div>
                      <div class="panel-body">
                          <form role="form" method="post" action="" id="form" enctype="multipart/form-data" data-parsley-validate>
                              <div class="form-group">
                                    <input type="name" name="nome" class="form-control input-sm" placeholder="NOME COMPLETO" autofocus required>
                              </div>
                              <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="cpf" class="form-control input-sm cpf" placeholder="CPF" data-parsley-cpf data-parsley-trigger="blur" maxlength="14" required>
                                        </div>
                                    </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" name="rg" class="form-control input-sm rg" placeholder="RG" maxlength="12" required>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6" >
                                      <div class="form-group">
                                          <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" name="datanasc" class="form-control input-sm" placeholder="DATA DE NASCIMENTO" >
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                            <input type="number" name="idade" class="form-control input-sm" placeholder="IDADE" data-parsley-trigger="blur">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <select class="form-control" name="sexo" required>
                                              <option value="" hidden selected disabled>SEXO</option>
                                              <option value="Feminino">Feminino</option>
                                              <option value="Masculino">Masculino</option>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                            <input type="text" name="profissao" class="form-control input-sm" placeholder="PROFISSÃO">
                                      </div>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <input type="text" name="endereco" class="form-control input-sm" placeholder="ENDEREÇO">
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <select class="form-control" name="estadocivil">
                                              <option value="" hidden selected disabled>ESTADO CIVÍL</option>
                                              <option value="Casado" >Casado</option>
                                              <option value="Solteiro ">Solteiro</option>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                            <input type="text" name="cnh" class="form-control input-sm" placeholder="CNH">
                                      </div>
                                  </div>
                              </div>
                               <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" name="cep" placeholder="CEP" class="form-control input-sm cep">
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" name="cidade"  placeholder="CIDADE" class="form-control input-sm">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" name="bairro" placeholder="BAIRRO" class="form-control input-sm">
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" name="uf"  placeholder="UF" class="form-control input-sm">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" name="telefone" placeholder="TELEFONE" class="form-control input-sm phone">
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="email" name="email" placeholder="EMAIL" class="form-control input-sm" data-parsley-type="email">
                                      </div>
                                  </div>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputFile" style="font-size: 15px;">Imagem da CNH</label>
                                <input type="file" id="exampleInputFile" name="image" required>
                              </div>
 
                              </div>
                          </div>
                      </div>
                  </div>

            <div class="container pdbot">
              <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3 bgForm">
                      <div class="panel-heading">
                          <h3 class="panel-title text-center" style="font-family:Open Sans; color:white; font-size: 25px;">Dados do Automóvel</h3>
                      </div>
                      <div class="panel-body">                      
                              <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="fabricanteCarro" class="form-control input-sm" placeholder="FABRICANTE" required>
                                        </div>
                                    </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" name="modeloCarro" class="form-control input-sm" placeholder="MODELO" required>
                                      </div>
                                  </div>
                              </div>
                               <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <select class="form-control" name="combustivelCarro" required>
                                              <option value="" hidden selected disabled>Combustível</option>
                                              <option value="Gasolina">Gasolina</option>
                                              <option value="Alcool">Alcool</option>
                                              <option value="Gasolina/Alcool">Gasol / Alcool</option>
                                              <option value="Kit Gas">Kit Gás</option>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                       <div class="form-group">
                                          <select class="form-control" name="nPortasCarro" required>
                                              <option value="" hidden selected disabled>N° DE PORTAS</option>
                                              <option value="2">2</option>
                                              <option value="3">3</option>
                                              <option value="4">4</option>
                                              <option value="5">5</option>
                                          </select>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" name="anoCarro" class="form-control input-sm year" placeholder="ANO/MODELO" required>
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                            <input type="text" name="placaCarro" class="form-control input-sm placa" placeholder="PLACA" data-parsley-trigger="blur" maxlength="8">
                                      </div>
                                  </div>
                              </div>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="container pdbot">
                  <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3 bgForm">
                      <div class="panel-heading">
                          <h3 class="panel-title text-center" style="font-family:Open Sans; color:white; font-size: 25px;">Perfil do usuário</h3>
                      </div>
                      <div class="panel-body">
                              <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                          <select class="form-control" name="usoCarro" required>
                                              <option value="" hidden selected disabled>USO DO VEÍCULO</option>
                                              <option value="Particular">Particular</option>
                                              <option value="Trabalho">Trabalho</option>
                                              <option value="Faculdade">Faculdade</option>
                                              <option value="Particular/Trabalho">Particular/Trabalho</option>
                                              <option value="Particular/Faculdade">Particular/Faculdade</option>


                                          </select>
                                      </div>
                                    </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <select class="form-control" name="garagemCarro" required>
                                              <option value="" hidden selected disabled>Garagem/Estacionamento</option>
                                              <option value="Nao Possui">Não Possui</option>
                                              <option value="Trabalho">No Trabalho</option>
                                              <option value="Residencia">Na Residência</option>
                                              <option value="Faculdade">Na Faculdade</option>
                                          </select>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                <div class="col-md-12">
                                  <label>A garagem possui portão?</label> &nbsp;&nbsp;&nbsp;&nbsp;
                                  <label class="radio-inline"><input type="radio" name="possuiPortao" value="sim">Sim</label>
                                  <label class="radio-inline"><input type="radio" name="possuiPortao" value="nao">Não</label>
                                </div>
                              </div>
                              <div class="row pdbot">
                                <div class="col-md-12">
                                  <label>Possui alarme, bloqueador e rastreador?</label> &nbsp;&nbsp;&nbsp;&nbsp;
                                  <label class="radio-inline"><input type="radio" name="seguranca" value="sim">Sim</label>
                                  <label class="radio-inline"><input type="radio" name="seguranca" value="nao">Não</label>
                                </div>
                              </div>
                              
                              <div class="form-group text-center ">
                                  <input type="submit" value="Cadastrar" name="Cadastrar" class="btn btAlterarCadastrar">
                              </div>     
                              </div>
                          </div>
                      </div>
                  </div>
                  <input type="date" name="data" value="<?php echo date('Y-m-d'); ?>" hidden>
                    <input type="number" name="idFuncionario" value="<?php echo $_SESSION['idFuncionario']?>" hidden>
                </form>

        <script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/Parsley.js-2.3.5/dist/parsley.min.js"></script>
        <script type="text/javascript" src="js/parsleyjs-validators-master/dist/cpf.js"></script>
        <script type="text/javascript" src="js/parsleyjs-validators-master/dist/i18n/pt-br.js"></script>
        <script type="text/javascript" src="js/Parsley.js-2.3.5/dist/i18n/pt-br.js"></script>
        <script type="text/javascript" src="js/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js"> </script>
        <script type="text/javascript" src="js/bootstrap-sweetalert-master/lib/sweet-alert.js"></script>

        <?php
        if(isset($_SESSION['mensagem'])){
          ?>
          <script>
          $(document).ready(function() {
            swal("Sucesso", "Usuário cadastrado com sucesso!", "success")
          })
          </script>
           <?php unset($_SESSION['mensagem']);
        }

         ?>

        <script> $(document).ready(function(){
            $('.year').mask('0000');
            $('.time').mask('00:00:00');
            $('.date_time').mask('00/00/0000 00:00:00');
            $('.cep').mask('00000-000');
            $('.placa').mask('AAA-0000');
            $('.phone').mask('0000-0000');
            $('.phone_with_ddd').mask('(00) 0000-0000');
            $('.phone_us').mask('(000) 000-0000');
            $('.mixed').mask('AAA 000-S0S');
            $('.cpf').mask('000.000.000-00', {reverse: true});
            $('.rg').mask('00.000.000-0');
            $('.money').mask('000.000.000.000.000,00', {reverse: true});
}); 
        </script>
        <!-- Tratando telefones de São Paulo -->
        <script> 
            var maskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
                options = {onKeyPress: function(val, e, field, options) {
                    field.mask(maskBehavior.apply({}, arguments), options);
    }
};
            $('.phone').mask(maskBehavior, options); 
        </script>
        
         <script type="text/javascript">
            $('#form').parsley();
        </script>
        
    </body>
</html>