<?php 
    $pagina = @$_GET["p"];
    require_once ("../Controller/crudSeguradora.php");
?>

</html>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <script src="js/jquery-1.12.0.min.js"></script>    
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/bootstrap-sweetalert-master/lib/sweet-alert.js"></script>
        <link href="css/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/cavalaro.css" type="text/css" media="all" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="js/bootstrap-sweetalert-master/lib/sweet-alert.css">
        <link rel="stylesheet" type="text/css" href="css/estilo.css" media="print">
        
        <title>Cavalaro - Home</title>
    </head>
    
      <body class="bgMain">    
          <header>
            <?php include 'header.php';?>
          </header>

       <div class="container esconder">
       <a href="seguradora_listar.php?p=0">
            <i class="fa fa-arrow-circle-left fa-3x pull-left" aria-hidden="true" title="Voltar"></i>
         </a>
          <div class="col-md-2">
              <h4 style="color: white; text-align: right;">Filtrar por mês:</h4>
          </div>
          <form method="post" action="../View/seguradoraRelatorio.php?p=0">
              <div class="col-md-2 " >
                  <select class="form-control input-sm input" name="filtro">
                      <option value="1">Janeiro</option>
                      <option value="2">Fevereiro</option>
                      <option value="3">Março</option>
                      <option value="4">Abril</option>
                      <option value="5">Maio</option>
                  </select>
              </div>    
              <div class="col-md-4">
                  <input type="submit" name="btFiltrar" value="Filtrar" class="btn btn-info btl-lg btn-primary">
              </div>
          </form>
          <form method="post">
              <div class="col-md-2">
                  <input type="submit" name="btnGerarRelatorio" value="Imprimir" class="btn btn-info btn-lg btn-danger btExcliuir" onclick="javascript:print()" />
              </div>
          </form>
          </div>

        <div class="col-md-10 col-md-offset-1">
            <div class="panel-body">
              <table class="table table-hover table-bordered" id="printable">                
                <thead>
                  <tr>
                    <th>Nome Fantasia</th>
                    <th>CNPJ</th>
                    <th>Telefone</th>
                    <th>Representante</th>
                    <th>Email</th>
                    <th>Endereço</th>
                    <th>Bairro</th>
                    <th>CEP</th>
                    <th>UF</th>
                  </tr>
              </thead>
              <tbody>

                <?php foreach($dados as $row){ 
                    $idSeguradora = $row['idSeguradora'];
                    $nomeSeguradora = $row['nomeSeguradora'];
                    $razao = $row['razaoSocial'];
                    $cnpj = $row['cnpj'];
                    $telefoneSeguradora = $row['telefoneSeguradora'];
                    $homepage = $row['homepage'];
                    $representante = $row['representante'];
                    $email = $row['emailSeguradora'];
                    $logradouro = $row['logradouroSeguradora'];
                    $bairro = $row['bairroSeguradora'];
                    $cep = $row['cepSeguradora'];
                    $uf = $row['ufSeguradora'];
                ?>

                <tr>

                    <td><?php echo $nomeSeguradora ?> </td>
                    <td><?php echo $cnpj ?> </td>
                    <td><?php echo $telefoneSeguradora ?> </td>
                    <td><?php echo $representante; ?> </td>
                    <td><?php echo $email ?> </td>
                    <td><?php echo $logradouro ?> </td>
                    <td><?php echo $bairro ?> </td>
                    <td><?php echo $cep ?></td>
                    <td><?php echo $uf; ?> </td>

                </tr>

                <?php } ?>
              </tbody>
          </table>  
          <div class="panel-footer esconder">
                <div class="row">
                  <div class="col col-xs-4">Page 1 of 5
                  </div>
                  <div class="col col-xs-8">
                    <ul class="pagination hidden-xs pull-right">
                     <?php 
                      $limite = 10; 
                      $totalPaginas = $qtdPag/$limite;

                      for($c=0;$c<=$totalPaginas;$c++){
                        echo '<li><a href="listarSeguradora.php?p='.$c.'">'.$c.'</a></li>';
                      }
                    ?>
              </ul>
              <ul class="pagination visible-xs pull-right">
                  <li><a href="#">«</a></li>
                  <li><a href="#">»</a></li>
              </ul>
            </div>
          </div>
        </div>
        </div>
      </div>
    </body>

</html>

