<?php
    require_once ("../Controller/crudPagamentoContas.php");
?>
<!DOCTYPE html>

    <html>
    <head>
        <meta charset="utf-8">
        
        <link href="css/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/cavalaro.css" type="text/css" media="all" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/estilo.css" media="print">
        <script src="js/jquery-1.12.0.min.js"></script>    
        
        <title>Cavalaro - Pagamento de Contas</title>
    </head>

        <body class="bgMain">
            <header>
              <?php include 'header.php';?>
            </header>
            
            <div class="container esconder">
            <a href="pagamentoContas.php?p=0">
                <i class="fa fa-arrow-circle-left fa-3x pull-left" aria-hidden="true" title="Voltar"></i>
            </a>
                <div class="col-md-2">
                    <h4>Filtrar por mês:</h4>
                </div>
            <form method="post" action="../View/pagamentoContas_relatorio.php">
                <div class="col-md-2">
                    <select class="form-control input-sm input" name="filtro">
                        <option value="1">Janeiro</option>
                        <option value="2">Fevereiro</option>
                        <option value="3">Março</option>
                        <option value="4">Abril</option>
                        <option value="5">Maio</option>
                    </select>
                </div>    
                <div class="col-md-4">
                    <input type="submit" name="btFiltrar" value="Filtrar" class="btn btn-info btl-lg btn-primary">
                </div>
            </form>
            <form method="post">
                <div class="col-md-2">
                    <input type="submit" name="btnGerarRelatorio" value="Imprimir" class="btn btn-info btn-lg btn-danger btExcliuir" onclick="javascript:print()" />
                </div>
            </form>
            </div>
           

            <div class="container" id="printable" >
                <div class="panel-body" >
                <table class="table table-hover table-bordered" >
                <thead>
                    <tr>
                        <th class="text-center">Descrição do pagamento</th>
                        <th class="text-center">Valor</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td align="right">Valor Total</td>
                        <td align="center">R$ <?php echo $sum; ?></td>
                    </tr>
                </tfoot>
                    <?php 
                        if (!empty($busca)) {
                          foreach($busca as $row){ 
                    
                    ?>

                    <tr>
                        <td align="center"><?php echo $row['descricaoPagamento']?></td>
                        <td align="center">R$ <?php echo $row['valorPagamento'] ?></td>
                    </tr>

                    <?php }} ?>

                </table>
                </div>
            </div>

            <script>
                function print(){
                    $(#printable).printElement();
                 }
       </script>
        
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>        
    </body>
</html>     