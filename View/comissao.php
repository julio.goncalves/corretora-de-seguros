<?php
    
    require_once ('../Controller/crudComissao.php');

   $idFuncionario = $row['idFuncionario'];
   $nomeFuncionario = $row['nomeFuncionario'];
   $valor = $row['valorCom'];
   
if (isset($_POST['btImprimir'])) {
$html = "
     <fieldset>
        <div class='container'>
        <div class='text-center'>
         <img src='img/logorelatorio.png' >
        </div>
        <br>
        <h3>Comissões</h3>
         <table class='table table-hover table-bordered'>
                <thead>
                    <tr>
                        <th class='text-center'>Código</th>
                        <th class='text-center'>Nome do Corretor</th>
                        <th class='text-center'>Mês</th>
                        <th class='text-center'>Valor(R$)</th>
                    </tr>
                </thead>                   
                  
                    <tr>
                        <td class='text-center'> $idFuncionario </td>
                        <td class='text-center'> $nomeFuncionario</td>
                        <td class='text-center'> $mes_nome </td>
                        <td class='text-center'>R$ $valor </td>
                    </tr>
                <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td align='right'>Total</td>
                        <td align='center'>R$ $sum </td>
                    </tr>
                </tfoot>
                </table>
            </div>
     </fieldset>";

    $mpdf=new mPDF(); 
    $mpdf->SetDisplayMode('fullpage');
    $css = file_get_contents("css/cavalaro.css");
    $css = file_get_contents("css/bootstrap-3.3.6-dist/css/bootstrap.css");
    $mpdf->WriteHTML($css,1);
    $mpdf->WriteHTML($html);
    $mpdf->Output("comissao.pdf",'D');
}

?>

<!DOCTYPE html>

    <html>
    <head>
        <meta charset="utf-8">
        
        <link href="css/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/cavalaro.css" type="text/css" media="all" rel="stylesheet">
        
        <title>Cavalaro - Pagamento de Contas</title>
    </head>
    <body class="bgMain">  
        <header>
          <?php include 'header.php';?>
        </header>
            
            <div class="container">
                <div class="col-md-2">
                    <h4 style="color:white;">Filtrar por mês:</h4>
                </div>
            <form method="post">
                <div class="col-md-2 " >
                    <select class="form-control input-sm input" name="filtro">
                        <option value="1">Janeiro</option>
                        <option value="2">Fevereiro</option>
                        <option value="3">Março</option>
                        <option value="4">Abril</option>
                        <option value="5">Maio</option>
                    </select>
                </div>    
                <div class="col-md-4 ">
                    <input type="submit" name="btFiltrar" value="Filtrar" class="btn btn-info btl-lg btn-primary">
                </div>
                <div class="col-md-4 ">
                    <input type="submit" name="btImprimir" value="Imprimir" class="btn btn-info btl-lg btn-danger"></input>
                    
                </div>
            </form>
            </div>
           
            <div class="container">
                <div class="panel-body">
                <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th></th>
                        <th class="text-center">Nome do Corretor</th>
                        <th class="text-center">Mês</th>
                        <th class="text-center">Valor(R$)</th>
                    </tr>
                </thead>

                 <?php foreach($busca as $row){ 
                    $idFuncionario = $row['idFuncionario'];
                    $nomeFuncionario = $row['nomeFuncionario'];
                  ?>

                    <tr>
                        <td align="center"> 
                            <a href="comissao_detalhar.php?id=<?php echo $row['idComissao']; ?>">
                                <button class="btn btnAlterar" value="Exibir" name="Alterar">
                                    <em class="fa fa-th-large"></em>
                                </button>
                            </a>
                        </td>
                        <td class="text-center"><?php echo $nomeFuncionario; ?></td>
                        <td class="text-center"><?php echo $mes_nome; ?></td>
                        <td class="text-center">R$ <?php echo $row['valorCom']?></td>
                    </tr>
                   <?php } ?>

                <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td align="right">Total</td>
                        <td align="center">R$ <?php echo $sum;?></td>
                    </tr>
                </tfoot>

                </table>
                </div>

            </div>
        
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>        
    </body>
</html>