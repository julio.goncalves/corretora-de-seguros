<?php
require_once ("../Controller/crudClienteFisico.php");
$idCliente = $_GET['idCliente'];

?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">

        <link href="css/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/cavalaro.css" type="text/css" media="all" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="js/bootstrap-sweetalert-master/lib/sweet-alert.css">

        <title>Cliente Fisico</title>
    </head>
    <body class="bgMain">
    <header>
      <?php include 'header.php';?>
    </header>

    <div class="container">
              <div class="row">
                <a href="clienteFisico_listar.php?p=0">
               <i class="fa fa-arrow-circle-left fa-3x pull-left" aria-hidden="true" title="Voltar"></i>
              </a> 
                  <div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3 bgForm">
                      <div class="panel-heading">
                          <h3 class="panel-title text-center" style="font-family:Open Sans; color:white; font-size: 25px;">Dados do Segurado</h3>
                      </div>
                      <div class="panel-body">
                          <form role="form" method="post" action="../Controller/crudClienteFisico.php">

                          <div class="form-group">
      			    				     <input type="name" name="nome" class="form-control input-sm" placeholder="" value="<?php echo $row['nomeCliente'] ; ?>">
                          </div>

                          <div class="row">
      			    				   <div class="col-xs-6 col-sm-6 col-md-6">
      			    					    <div class="form-group">
                                  <input type="text" name="cpf" class="form-control input-sm" data-parsley-cpf data-parsley-trigger="blur" data-parsley-required value="<?php echo $row['cpfCliente'] ; ?>">
      			    				     	</div>
      			    				   </div>
                           
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="rg" class="form-control input-sm" value="<?php echo $row['rgCliente'] ; ?>">
                                </div>
                            </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" name="datanasc" class="form-control input-sm" placeholder="DATA DE NASCIMENTO" value="<?php echo $row['dataNascimento'] ; ?>">
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
      			    						<input type="text" name="idade" class="form-control input-sm" value="<?php echo $row['idade'] ; ?>">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <select class="form-control" name="sexo">
                                              <option value="<?php echo $row['sexoCliente']; ?>"  hidden><?php echo $row['sexoCliente']; ?></option>
                                              <option value="F">Feminino</option>
                                              <option value="M">Masculino</option>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
      			    						<input type="text" name="profissao" class="form-control input-sm" value="<?php echo $row['profissao'] ; ?>">
                                      </div>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <input type="text" name="endereco" class="form-control input-sm" value="<?php echo $row['enderecoPessoa'] ; ?>">
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <select class="form-control" name="estadocivil">
                                              <option value="<?php echo $row['estadocivil']; ?>" hidden><?php echo $row['estadocivil']; ?></option>
                                              <option value="Casado" >Casado</option>
                                              <option value="Solteiro ">Solteiro</option>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
      			    						<input type="text" name="cnh" class="form-control input-sm" value="<?php echo $row['cnhPessoa'] ; ?>">
                                      </div>
                                  </div>
                              </div>
                               <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" name="cep"  value="<?php echo $row['cepPessoa']; ?>" class="form-control input-sm">
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" name="cidade"  value="<?php echo $row['cidadePessoa']; ?>" class="form-control input-sm">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" name="bairro" value="<?php echo $row['bairroPessoa']; ?>" class="form-control input-sm">
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" name="uf"  value="<?php echo $row['ufPessoa']; ?>" class="form-control input-sm">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" name="telefone" value="<?php echo $row['telefonePessoa']; ?>" class="form-control input-sm">
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                          <input type="text" name="email" value="<?php echo $row['emailPessoa']; ?>" class="form-control input-sm">
                                      </div>
                                  </div>
                              </div>
                              <input type="text" name="idClienteFisico" hidden value="<?php echo $_GET['idCliente'] ?>">
                              
                              <div class="form-group text-center">
                                  <input type="submit" value="Alterar" name="btnAlterar" class="btn btAlterarCadastrar">
                              </div>
                          </form>
                      </div>
                  </div>
          </div>
        </div>

        <script src="js/jquery-1.12.0.min.js"></script>
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/parsleyjs-validators-master/dist/cpf.js"></script>
        <script type="text/javascript" src="js/parsleyjs-validators-master/dist/i18n/pt-br.js"></script>
        <script type="text/javascript" src="js/bootstrap-sweetalert-master/lib/sweet-alert.js"></script>
        
        <?php 
        // Alerta sucesso ao alterar cliente
        if(isset($_SESSION['mensagem'])){ 
        ?>
           <script>
               $(document).ready(function() {
                    swal("Sucesso", "Cliente alterado com sucesso!", "success")
               })
            </script>
  
          <?php unset($_SESSION['mensagem']); } ?>

    </body>
</html>