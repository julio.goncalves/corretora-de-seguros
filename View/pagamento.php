<?php

require_once ("../Controller/crudPagamento.php");

?>

<!DOCTYPE html>

    <html>
    <head>
        <meta charset="utf-8">
        
        <link href="css/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" media="all" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/font-awesome/css/font-awesome.min.css">
        <link href="css/cavalaro.css" type="text/css" media="all" rel="stylesheet">
        
        <title>Cavalaro - Pagamento</title>
    </head>

    <body class="bgMain">
        <header>
          <?php include 'header.php';?>
        </header>

            <div class="container">
            <form method="post" action="">
                <div class="col-md-3">
                    <input type="text" name="pesquisar" placeholder="Pesquisar" class="form-control input-sm input text-center" required="">
                </div>
                <div class="col-md-2">
                    <input type="submit" name="btnPesquisar" value="Pesquisar" class="btn btn-info btn-lg btn-primary btAlterarCadastrar" />
                </div>
            </form>
            </div>
           
            <div class="container">
                <div class="panel-body">
                <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th></th>
                        <th class="text-center">Código</th>
                        <th class="text-center">Nome Cliente</th>
                        <th class="text-center">Pagamento</th>
                        <th class="text-center">Valor</th>
                    </tr>
                </thead>
                <?php foreach($dados as $row){ 

                 ?>
                    <tr>
                       <td> 
                            <a href="seguro_detalhar.php?id=<?php echo $row['idClienteFisico']; ?>">
                                <button class="btn btnAlterar" value="Exibir" name="Alterar">
                                    <em class="fa fa-th-large"></em>
                                </button>
                            </a>
                        </td>
                            <td align="center"><?php echo $row['idSeguroCarro']; ?></td> 
                            <td align="center"><?php echo $row['nomeCliente']?></td>
                            <td align="center"><?php echo $row['tipoPagamento'] ?></td>
                            <td align="center">R$ <?php echo $row['valor']?></td>
                       
                    </tr>
                    <?php } ?>
                </table>
                <?php 
                    if (empty($dados)) { ?>

                    <div>
                         <p class="text-center" style="color:#EEE; font-size:25px;">Nenhum pagamento pendente!</p>
                    </div>   
                    <?php
                    }

                    ?>
                </div>
            </div>
        
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>        
    </body>
</html>