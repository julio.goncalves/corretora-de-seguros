<?php
require_once ("../Controller/crudFuncionario.php");
$idFuncionario = $_GET['idFuncionario'];

?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">

        <link href="css/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/cavalaro.css" type="text/css" media="all" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="js/bootstrap-sweetalert-master/lib/sweet-alert.css">

        <title>Alterar Funcionário</title>
    </head>
    <body class="bgMain">
    <header>
      <?php include 'header.php';?>
    </header>
    
      <div class="container">
      <a href="funcionario_listar.php?p=0">
               <i class="fa fa-arrow-circle-left fa-3x pull-left" aria-hidden="true" title="Voltar"></i>
              </a>
      <div class=" col-md-offset-3 ">
           </div>
              <div class="row">

                  <div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3 bgForm">
                      <div class="panel-heading">
                          <h3 class="panel-title text-center" style="font-family:Open Sans; color:white; font-size: 25px;">Dados do Funcionário</h3>
                      </div>
                      <div class="panel-body panel-form">
                        <form role="form" method="post" action="../Controller/crudFuncionario.php" >

                      <div class="form-group">
                        <input type="text" name="nome" placeholder="" class="form-control input-sm" required="" value="<?php echo $row['nomeFuncionario'];?>">
                      </div>

                      <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="text" name="cpf" placeholder="CPF" class="form-control input-sm" required="" value="<?php echo $row['cpfFuncionario'];?>">
                          </div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="text" name="rg" placeholder="RG" class="form-control input-sm" required="" value="<?php echo $row['rgFuncionario'];?>">
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="text" name="dataNasc" placeholder="DATA DE NASCIMENTO" class="form-control input-sm" value="<?php echo $row['dataNascimento'];?>">
                          </div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="text" name="cep" placeholder="CEP" class="form-control input-sm" value="<?php echo $row['cepPessoa'];?>">
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                            <input type="text" name="endereco"  placeholder="ENDEREÇO" class="form-control input-sm" value="<?php echo $row['enderecoPessoa'];?>">
                      </div>

                      <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                              <input type="text" name="permissao" placeholder="PERMISSAO" class="form-control input-sm" required="" value="<?php echo $row['permissao'];?>">
                          </div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="number" name="salario" placeholder="SÁLARIO" class="form-control input-sm" required="" value="<?php echo $row['salario'];?>">
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="text" name="bairro" placeholder="BAIRRO" class="form-control input-sm" value="<?php echo $row['bairroPessoa'];?>">
                          </div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="text" name="uf"  placeholder="UF" class="form-control input-sm" value="<?php echo $row['ufPessoa'];?>">
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="text" name="telefone" placeholder="TELEFONE" class="form-control input-sm" value="<?php echo $row['telefonePessoa'];?>">
                          </div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="text" name="email" placeholder="EMAIL" class="form-control input-sm" value="<?php echo $row['emailPessoa'];?>">
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="text" name="sexo" placeholder="SEXO FUNCIONARIO" class="form-control input-sm" required="" value=" <?php echo $row['sexoFuncionario'];?>">
                          </div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="password" name="senha" placeholder="SENHA" class="form-control input-sm" required="" value="<?php echo $row['senha'];?>">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="text" name="cidade" class="form-control input-sm" placeholder="CIDADE" value="<?php echo $row['cidadePessoa'];?>">
                          </div>
                        </div>
                      </div>
                        <div class="col-md-offset-5 pdtop">
                            <input type="submit" name="btnAlterar" value="Alterar" class="btn btn-info btn-lg btAlterarCadastrar">
                        </div>
                         <input type="text" name="idFuncionario" hidden value="<?php echo $_GET['idFuncionario'] ?>">
                    </form>
                      </div>
                  </div>
            </div>
        </div>
     
        <script src="js/jquery-1.12.0.min.js"></script>
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/parsleyjs-validators-master/dist/cpf.js"></script>
        <script type="text/javascript" src="js/parsleyjs-validators-master/dist/i18n/pt-br.js"></script>
        <script type="text/javascript" src="js/bootstrap-sweetalert-master/lib/sweet-alert.js"></script>
        

        <?php 
        // Alerta sucesso ao alterar seguradora
        if(isset($_SESSION['mensagem'])){ 
        ?>
           <script>
               $(document).ready(function() {
                    swal("Sucesso", "Seguradora alterado com sucesso!", "success")
               })
            </script>
  
          <?php unset($_SESSION['mensagem']); } ?>

    </body>
</html>