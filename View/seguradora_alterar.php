<?php
require_once ("../Controller/crudSeguradora.php");
$idSeguradora = $_GET['idSeguradora'];

?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <link href="css/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/cavalaro.css" type="text/css" media="all" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="js/bootstrap-sweetalert-master/lib/sweet-alert.css">
        <title>Alterar Seguradora</title>
    </head>

    <body class="bgMain">
        <header>
          <?php include 'header.php';?>
      </header>
        <div class="container">
        <a href="seguradora_listar.php?p=0">
               <i class="fa fa-arrow-circle-left fa-3x pull-left" aria-hidden="true" title="Voltar"></i>
              </a>
              <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3 bgForm">
                      <div class="panel-heading">
                          <h3 class="panel-title text-center" style="font-family:Open Sans; color:white; font-size: 25px;">Dados da Seguradora</h3>
                      </div>
                      <div class="panel-body panel-form">
                        <form role="form" method="post" action="../Controller/crudSeguradora.php" >
                          <div class="form-group">
                            <input type="text" name="nomeFantasia" placeholder="NOME FANTASIA" class="form-control input-sm" required="" value="<?php echo $row['nomeSeguradora'];?>">
                          </div>

                        <div class="form-group">
                                <input type="text" name="razaoSocial" placeholder="RAZÃO SOCIAL" class="form-control input-sm" required="" value="<?php echo $row['razaoSocial'];?>">
                        </div>

                        <div class="form-group">
                                <input type="text" name="representante" placeholder="REPRESENTANTE" class="form-control input-sm" required="" value="<?php echo $row['representante'];?>">
                        </div>

                          <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                              <div class="form-group">
                                <input type="text" name="cnpj" placeholder="CNPJ" class="form-control input-sm" required="" value="<?php echo $row['cnpj'];?>">
                              </div>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6">
                              <div class="form-group">
                                <input type="text" name="cep" placeholder="CEP" class="form-control input-sm" required="" value="<?php echo $row['cepSeguradora'];?>">
                              </div>
                            </div>
                          </div>

                           <div class="form-group">
                                <input type="text" name="endereco"  placeholder="ENDEREÇO" class="form-control input-sm" value="<?php echo $row['logradouroSeguradora'];?>">
                            </div>

                          <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                              <div class="form-group">
                                <input type="text" name="cidade" placeholder="CIDADE" class="form-control input-sm" required="" value="<?php echo $row['cidadeSeguradora'];?>">
                              </div>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6">
                              <div class="form-group">
                                <input type="text" name="website" placeholder="WEBSITE" class="form-control input-sm" required="" value="<?php echo $row['homepage'];?>">
                              </div>
                            </div>
                          </div>
                           <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                              <div class="form-group">
                                <input type="text" name="bairro" placeholder="BAIRRO" class="form-control input-sm" required="" value="<?php echo $row['bairroSeguradora'];?>">
                              </div>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6">
                              <div class="form-group">
                                <input type="text" name="uf" placeholder="UF" class="form-control input-sm" required="" value="<?php echo $row['ufSeguradora'];?>">
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                              <div class="form-group">
                                <input type="text" name="telefone" placeholder="TELEFONE" class="form-control input-sm" required="" value="<?php echo $row['telefoneSeguradora'];?>">
                              </div>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6">
                              <div class="form-group">
                                <input type="text" name="email" placeholder="E-MAIL" class="form-control input-sm" required="" value="<?php echo $row['emailSeguradora'];?>">
                              </div>
                            </div>
                            <div class="col-md-offset-5 pdtop">
                                <input type="submit" name="btnAlterar" value="Alterar" class="btn btn-info btn-lg btAlterarCadastrar">
                            </div>
                          </div>
                          </div>
                          <input type="text" name="idSeguradora" hidden value="<?php echo $_GET['idSeguradora'] ?>">
                        </form>
                      </div>
                  </div>
          </div>
        </div>
     
        <script src="js/jquery-1.12.0.min.js"></script>
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/parsleyjs-validators-master/dist/cpf.js"></script>
        <script type="text/javascript" src="js/parsleyjs-validators-master/dist/i18n/pt-br.js"></script>
        <script type="text/javascript" src="js/bootstrap-sweetalert-master/lib/sweet-alert.js"></script>
        
        <?php 
        // Alerta sucesso ao alterar cliente
        if(isset($_SESSION['mensagem'])){ 
        ?>
           <script>
               $(document).ready(function() {
                    swal("Sucesso", "Seguradora alterada com sucesso!", "success")
               })
            </script>
  
          <?php unset($_SESSION['mensagem']); } ?>

    </body>
</html>