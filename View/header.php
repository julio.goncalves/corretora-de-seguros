 <div>
 <link rel="stylesheet" type="text/css" href="css\font-awesome\css\font-awesome.css">
 <script src="js/jquery-1.12.0.min.js"></script>
 <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
<link href="css/cavalaro.css" type="text/css" media="all" rel="stylesheet">
 
 <?php 
    @session_start();
    if($_SESSION['logado']!=1){
      header('location: login.php');
}
    $idFunc = $_SESSION['idFuncionario'];
    $nomeFunc = $_SESSION['nomeFuncionario'];
    $perm = $_SESSION['permissao'];
    $pos_espaco = strpos($nomeFunc, ' '); 
    $primeiro_nome = substr($nomeFunc, 0, $pos_espaco);   
    
    
 ?>
            <div class="container-fluid esconder">
                <div class="row">
                    <div class="col-sm-7 col-md-7">
                        <a href="index.php">
                        <img src="img/logo.fw.png" alt="logo" class="pdtop logo pull-right" style="padding-top:15px;">
                        </a>
                    </div>
                    <div class="col-sm-5 col-md-5">
                        <span class="hidden-xs hidden-sm" style="padding-left: 150px;"><i class="fa fa-user fa-3x" style="padding-top:15px;" aria-hidden="true"></i> Bem vindo, <a href="../Controller/logout.php" style="color: white;"> <?php echo$primeiro_nome; ?> </a></span>
                        <a class="hidden-xs hidden-sm" href="../Controller/logout.php" style="margin-top: 50px"><span title="Sair"><i class="fa fa-sign-out fa-3x pull-right" aria-hidden="true"></i></span></a>
                    </div>
                </div>
                
                <div class="col-lg-12 text-center hidden-xs hidden-sm" class="" style="padding-top: 20px;">
                    <hr class="primary">
                    <div class="row">
                        <a href="seguro_cadastrar.php" class="menu_ativo" id="seguro_cadastrar.php">
                            <button type="button" class="btn btn-default btn-lg mgleft mgright-menu btHome buttonMinHeight" id="seguro">Seguros</button>
                        </a>
                        <a href="simulacao_listar.php">
                        <button type="button" class="btn btn-default btn-lg mgright-menu btHome buttonMinHeight" id="simulacao">Simulações</button>
                        </a>
                        <a href="pagamento.php">
                            <button type="button" class="btn btn-default btn-lg mgright-menu buttonMinHeight btHome" id="pagamento" >Pagamentos</button>
                        </a>
                        <a href="funcionario_listar.php?p=0">
                            <button type="button" class="btn btn-default btn-lg mgright-menu btHome buttonMinHeight" id="funcionario">Funcionários</button>                            
                        </a>
                        <a href="clienteFisico_listar.php?p=0">
                            <button type="button" class="btn btn-default btn-lg mgright-menu btHome buttonMinHeight" id="cliente">Clientes</button>
                        </a>
                         <a href="seguradora_listar.php?p=0">
                            <button type="button" class="btn btn-default btn-lg mgright-menu btHome buttonMinHeight" id="seguradora">Seguradoras</button>
                        </a>
                        <a href="pagamentoContas.php">
                            <button type="button" class="btn btn-default btn-lg mgright-menu btHome buttonMinHeight" id="conta">Contas</button>
                        </a>
                        <a href="comissao.php">
                            <button type="button" class="btn btn-default btn-lg mgright-menu buttonMinHeight btHome" id="comissao">Comissões</button>
                        </a>
                        
                    </div>
                     <hr class="primary">
                </div>
                <nav class="navbar navbar-default hidden-md hidden-lg">
                  <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav nav-pills">
                            <li><a href="#">Olá, <?php echo $primeiro_nome ?> </a></li>
                            <li><a href="#">Seguro</a></li>
                            <li><a href="#">Simulações</a></li>
                            <li><a href="#">Pagamentos</a></li>
                            <li><a href="#">Funcionários</a></li>
                            <li><a href="#">Seguradora</a></li>
                            <li><a href="#">Contas &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</a></li>
                            <li><a href="#">Comissões &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;</a></li>
                            <li class="hidden-md hidden-lg"><a href="../Controller/logout.php">Sair</a></li>
                            
                          </ul>
                        </li>
                      </ul>
                      
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>
            </div>
                <script>
                <?php
                if ($perm == 'financeiro') { ?>    
                    $('#simulacao').attr('disabled', true);
                    $('#seguro').attr('disabled', true);
                    $('#funcionario').attr('disabled', true);
                    $('#cliente').attr('disabled', true);
                    $('#seguradora').attr('disabled', true);
                <?php  } 
                if ($perm == 'corretor') { ?>
                    $('#funcionario').attr('disabled', true);
                    $('#conta').attr('disabled', true);
                    $('#comissao').attr('disabled', true);  
                    $('#pagamento').attr('disabled', true);               
            <?php } ?>

            </script>
        </div>
