<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        
        <link href="css/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/cavalaro.css" type="text/css" media="all" rel="stylesheet">
        
        <title>Cavalaro - Home</title>
    </head>
    <body class="bgMain">  
    <header>
      <?php include 'header.php';?>
    </header>
        
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        
    </body>
</html>