// Generate HTML files from handlebars source
module.exports = {
  options: {
    data: [
      '<%= config.src %>/templates/data/*.yml',
      'package.json'
    ],
    helpers: [
      // '<%= config.src %>/templates/helpers/*.js'
    ],
    layoutdir: '<%= config.src %>/templates/layouts',
    layout: false,
    partials: [
      '<%= config.src %>/templates/{layouts,partials}/{,*/}*.hbs'
    ],
    flatten: true
  },
  server: {
    files: [{
      expand: true,
      cwd: '<%= config.src %>/templates/pages',
      src: ['{,*/}*.hbs'],
      dest: '.tmp',
      ext: '.html'
    }]
  },
  docs: {
    files: [{
      expand: true,
      cwd: '<%= config.src %>/templates/pages',
      src: ['{,*/}*.hbs'],
      dest: '<%= config.docs %>',
      ext: '.html'
    }]
  }
};
