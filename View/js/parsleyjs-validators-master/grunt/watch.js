// Watches files for changes and runs tasks based on the changed files
module.exports = {
  bower: {
    files: ['bower.json'],
    tasks: ['wiredep']
  },
  js: {
    files: ['<%= config.src %>/scripts/{,*/}*.js'],
    tasks: ['jshint'],
    options: {
      livereload: true
    }
  },
  jstest: {
    files: ['test/spec/{,*/}*.js'],
    tasks: ['test:watch']
  },
  gruntfile: {
    files: ['Gruntfile.js']
  },
  sass: {
    files: [
      '<%= config.src %>/styles/{,*/}*.{scss,sass}'
    ],
    tasks: ['sass:server', 'autoprefixer']
  },
  styles: {
    files: ['<%= config.src %>/styles/{,*/}*.css'],
    tasks: ['newer:copy:styles', 'autoprefixer']
  },
  livereload: {
    options: {
      livereload: '<%= connect.options.livereload %>'
    },
    files: [
      '.tmp/{,*/}*.html',
      '.tmp/styles/{,*/}*.css',
      '<%= config.src %>/images/{,*/}*'
    ]
  },
  assemble: {
    files: [
      '<%= config.src %>/templates/{data,layouts,partials}/{,*/}*.{hbs,yml}'
    ],
    tasks: ['assemble:server']
  },
  pages: {
    files: [
      '<%= config.src %>/templates/pages/{,*/}*.hbs'
    ],
    tasks: ['assemble:server']
  }
};
