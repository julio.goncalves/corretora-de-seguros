// Run some tasks in parallel to speed up build process
module.exports = {
  server: [
    'assemble:server',
    'sass:server',
    'copy:styles'
  ],
  test: [
    'copy:styles'
  ],
  docs: [
    'sass:docs',
    'copy:styles',
    'imagemin:docs',
    'svgmin:docs'
  ]
};
