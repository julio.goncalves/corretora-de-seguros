// Automatically inject Bower components into the HTML file
module.exports = {
  src: {
    src: ['<%= config.src %>/templates/layouts/master.hbs'],
    ignorePath: /^\/|(\.\.\/){1,3}/,
    exclude: ['bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.js'],
    devDependencies: true,
    overrides: {
      'parsleyjs-validators': {
        main: [
          '../../dist/cnpj.js',
          '../../dist/cpf.js',
          '../../dist/date.js',
          '../../dist/file.js',
          '../../dist/ge.js',
          '../../dist/gt.js',
          '../../dist/hexcolor.js',
          '../../dist/isadult.js',
          '../../dist/le.js',
          '../../dist/lt.js',
          '../../dist/notequalto.js',
          '../../dist/time.js'
        ]
      }
    }
  },
  sass: {
    src: ['<%= config.src %>/styles/{,*/}*.{scss,sass}'],
    ignorePath: /(\.\.\/){1,2}bower_components\//,
    devDependencies: true
  }
};
