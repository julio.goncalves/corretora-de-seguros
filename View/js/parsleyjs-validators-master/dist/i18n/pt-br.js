// ParsleyConfig definition if not already set
window.ParsleyConfig = window.ParsleyConfig || {};
window.ParsleyConfig.i18n = window.ParsleyConfig.i18n || {};

// Define then the messages
window.ParsleyConfig.i18n['pt-br'] = $.extend(window.ParsleyConfig.i18n['pt-br'] || {}, {
  cnpj: 'Este campo deve ser um CNPJ válido',
  cpf: 'Este campo deve ser um CPF válido',
  date: 'Este campo deve ser uma data válida',
  filetype: 'O arquivo selecionado deve ser um arquivo %s.',
  ge: 'Este campo deve ser maior ou igual.',
  gt: 'Este campo deve ser maior.',
  hexcolor: 'Este campo deve ser uma cor hexadecimal válida',
  isadult: 'Para participar você precisa ter pelo menos %s anos de idade.',
  le: 'Este campo deve ser menor ou igual.',
  lt: 'Este campo deve ser menor.',
  notequalto: 'Este campo não deve ser o mesmo.',
  time: 'Este campo deve ser uma hora válida.'
});

// If file is loaded after Parsley main file, auto-load locale
if ('undefined' !== typeof window.ParsleyValidator) {
  window.ParsleyValidator.addCatalog('pt-br', window.ParsleyConfig.i18n['pt-br'], true);
}
