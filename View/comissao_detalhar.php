	<?php
    $idComissao = $_GET["id"];
    require_once ("../Controller/crudComissao.php");
    

      $nome = $row['nomeCliente'];
      $cpf = $row['cpfCliente']; 
      $rg = $row['rgCliente'];
      $sexo = $row['sexoCliente'];
      $dataNascimento = $row['dataNascimento'];
      $idade = $row['idade'];
      $profissao = $row['profissao']; 
      $estadocivil = $row['estadocivil']; 
      $marca = $row['marca'];
      $modelo = $row['modelo'];
      $ano = $row['ano'];
      $placa = $row['placa'];
      $numeroPortas = $row['numeroPortas'];
      $usoVeiculo = $row['usoVeiculo'];
      $garagem = $row['garagem'];
      $seguranca = $row['seguranca'];
      $idSeguroCarro = $row['idSeguroCarro'];
      $valor = $row['valorCom'];
      $dataComissao = $row['dataComissao'];
      $valorSeg = $row['valor'];
      $tipoPagamento = $row['tipoPagamento'];

?>

</html>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <script src="js/jquery-1.12.0.min.js"></script>    
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/bootstrap-sweetalert-master/lib/sweet-alert.js"></script>
        <link href="css/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/cavalaro.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/table.css" type="text/css" media="all" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="js/bootstrap-sweetalert-master/lib/sweet-alert.css">
        
        <title>Cavalaro - Home</title>
    </head>
    <body class="bgMain">
      <header>
        <?php include 'header.php';?>
      </header>

      <div class="container">
         <div class="row">
         	<div class="pull-left">
            <a href="comissao.php" id="">
               <i class="fa fa-arrow-circle-left fa-3x " aria-hidden="true" title="Voltar" ></i>
              </a>
         	</div>
         </div><br> 
      <div class="row">
      <div class="col-md-6">
        <table>
            <tbody>
              <tr>
                <th>Nome Cliente</th>
                  <td><?php echo $nome ?></td>
              </tr>
              <tr>
                <th>CPF</th>
                  <td><?php echo $cpf ?></td>
              </tr>

              <tr>
                <th>RG</th>
                  <td> <?php echo $rg ?></td>
              </tr>
              <tr>
                <th>Sexo</th>
                  <td> <?php echo $sexo ?></td>
              </tr>
              <tr>
                <th>Data Nascimento</th>
                  <td><?php echo $dataNascimento ?></td>
              </tr>
              <tr>
                <th>Idade</th>
                  <td><?php echo $idade ?></td>
              </tr>
              <tr>
                  <th>Profissão</th>
                    <td> <?php echo $profissao ?></td>
                </tr>
                <tr>
                  <th>Estado Civil</th>
                    <td><?php echo $estadocivil ?></td>
                </tr>
                <tr>
                  <th>Valor do Seguro</th>
                    <td><?php echo $valorSeg ?></td>
                </tr>

                <tr>
                  <th>Valor da Comissão</th>
                    <td><?php echo $valor ?></td>
                </tr>
          </tbody>
        </table>
        </div>
        <div class="col-md-6">
	        <table >
	            <tbody>            
	               <tr>
	                <th>Marca</th>
	                  <td><?php echo $marca ?></td>
	              </tr>
	              <tr>
	                  <th>Modelo</th>
	                    <td> <?php echo $modelo ?></td>
	                </tr>
	                <tr>
	                  <th>Ano</th>
	                    <td><?php echo $ano ?></td>
	                </tr>
	                <tr>
	                  <th>Placa</th>
	                    <td><?php echo $placa ?></td>
	                </tr>
	                <tr>
	                  <th>Numero de Portas</th>
	                    <td><?php echo $numeroPortas ?></td>
	                </tr>
	                <tr>
	                  <th>Uso Veiculo</th>
	                    <td><?php echo $usoVeiculo ?></td>
	                </tr>
	                <tr>
	                  <th>Garagem</th>
	                    <td><?php echo $garagem ?></td>
	                </tr>
	                <tr>
	                  <th>Segurança</th>
	                    <td><?php echo $seguranca ?></td>
	                </tr>
	                <tr>
	                	<th>Data da Comissão</th>
	                	<td><?php echo $dataComissao ?></td>
	                </tr>
	                <tr>
	                	<th>Tipo de Pagamento</th>
	                	<td><?php echo $tipoPagamento	 ?></td>
	                </tr>
	          </tbody>
	        </table>
        </div> 
      </div>
    </div>
   </div>
           
		<script type="text/javascript">		    
		  <?php
		  if ($perm == 'corretor') { ?>
		      $('#cartao').attr('disabled', true);
		      $('#dinheiro').attr('disabled', true);
		      $('#boleto').attr('disabled', true);  
		      $('#desaparecer').attr('hidden', true);                 
		<?php } ?>

        </script>
    </body>    
</html>
