<!DOCTYPE html>
<?php
require_once ("../Controller/login.php");

if (@$_SESSION['logado'] == 1) {

     header('location: index.php');
}
?>

<html>
    <head>
        <meta charset="utf-8">

        <link href="css/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/cavalaro.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/login.css" type="text/css" media="all" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css\font-awesome\css\font-awesome.css">

        <title>Cavalaro - Login</title>
    </head>
    <body class="telaLogin">
        <header>

        </header>

        <div >
            <div class="container-fluid">
            <div class="col-xs-7 col-sm-4 col-md-4 col-lg-4 col-xs-offset-3 col-md-offset-4 bgForm pdtop" style="margin-top:15%;">
                <img src="img/logo.fw.png" class="center-block img-responsive" style="padding-bottom: 15px;">
                <form class="pdbot form-horizontal" action="" method="post">
                <?php if(isset($errMsg)){
                    echo '<div class="alert alert-danger"  role="alert">'.$errMsg.'.</div>';
                    } ?>
                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user " aria-hidden="true"></i></span>
                        <input id="login-username" type="text" class="form-control" name="emailFuncionario" placeholder="Email">
                    </div>
                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></span><input id="login-username" type="password" class="form-control" name="senha" placeholder="Senha">
                    </div>
                    <div class="text-center">
                    <button type="submit" class="btn btn-default btn-lg btLogin btn-block" name="btnSubmit">Login</button>
                    </div>
                </form>
             </div>
        </div>
        </div>

        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>

    </body>
</html>
