<?php
    require_once ("../Controller/crudFuncionario.php");
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">

        <script src="js/jquery-1.12.0.min.js"></script>    
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/bootstrap-sweetalert-master/lib/sweet-alert.js"></script>
        <link href="css/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/cavalaro.css" type="text/css" media="all" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="js/bootstrap-sweetalert-master/lib/sweet-alert.css">


          <title>Cavalaro - Funcionario</title>
    </head>
    <body class="bgMain">
        <header>
          <?php include 'header.php';?>
        </header>
        
          <div class="container">
            <a href="funcionario_listar.php?p=0">
               <i class="fa fa-arrow-circle-left fa-3x pull-left" aria-hidden="true" title="Voltar"></i>
              </a>
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3">
              		<div class="panel-heading painel-form">
      			    		<h3 class="col-md-offset-3 panel-title" style="font-family:Open Sans; color:white; font-size: 25px;" >Dados do Funcionário</h3>
      			 			</div>

      			 			<div class="panel-body painel-form">
      			    		<form role="form" method="post" action="../View/funcionario_cadastrar.php" >

      			    			<div class="form-group">
      			    				<input type="text" name="nome" placeholder="NOME COMPLETO" class="form-control input-sm" required="">
      			    			</div>

                      <div class="row">
      			    				<div class="col-xs-6 col-sm-6 col-md-6">
      			    					<div class="form-group">
      			                <input type="text" name="cpf" placeholder="CPF" class="form-control input-sm" data-parsley-cpf data-parsley-trigger="blur" required>
      			    					</div>
      			    				</div>

      			    				<div class="col-xs-6 col-sm-6 col-md-6">
      			    					<div class="form-group">
      			    						<input type="text" name="rg" placeholder="RG" class="form-control input-sm" required="">
      			    					</div>
      			    				</div>
      			    			</div>

                      <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="text" name="dataNasc" placeholder="DATA DE NASCIMENTO" class="form-control input-sm">
                          </div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="text" name="cep" placeholder="CEP" class="form-control input-sm">
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                            <input type="text" name="endereco"  placeholder="ENDEREÇO" class="form-control input-sm">
                      </div>

                      <div class="row">
                          <div class="col-xs-6 col-sm-6 col-md-6">
                              <div class="form-group">
                                  <select class="form-control" name="permissao" required>
                                      <option value="" hidden selected disabled>Permissão</option>
                                      <option value="financeiro">Financeiro</option>
                                      <option value="corretor">Corretor</option>
                                      <option value="administrador">Administrador</option>
                                  </select>
                              </div>
                          </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="number" name="salario" placeholder="SÁLARIO" class="form-control input-sm" required="">
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="text" name="bairro" placeholder="BAIRRO" class="form-control input-sm">
                          </div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="text" name="uf"  placeholder="UF" class="form-control input-sm">
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="text" name="telefone" placeholder="TELEFONE" class="form-control input-sm">
                          </div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="text" name="email" placeholder="EMAIL" class="form-control input-sm">
                          </div>
                        </div>
                      </div>

                      <div class="row">
                          <div class="col-xs-6 col-sm-6 col-md-6">
                              <div class="form-group">
                                  <select class="form-control" name="sexo" required>
                                      <option value="" hidden selected disabled>SEXO</option>
                                      <option value="feminino">Feminino</option>
                                      <option value="masculino">Masculino</option>
                                  </select>
                              </div>
                          </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="password" name="senha" placeholder="SENHA" class="form-control input-sm" required="">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <div class="form-group">
                            <input type="text" name="cidade" class="form-control input-sm" placeholder="CIDADE" >
                          </div>
                        </div>
                      </div>
                        <div class="col-md-offset-5 pdtop">
                            <input type="submit" name="btnSubmit" value="Cadastrar" class="btn btn-info btn-lg btAlterarCadastrar">
                        </div>
      			    		</form>
      			    	</div>
                </div>
          		</div>
		    		</form>
		    	</div>
    		</div>
  		</div>
  	</div>
  </div>
        <script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/Parsley.js-2.3.5/dist/parsley.min.js"></script>
        <script type="text/javascript" src="js/parsleyjs-validators-master/dist/cpf.js"></script>
        <script type="text/javascript" src="js/parsleyjs-validators-master/dist/i18n/pt-br.js"></script>
        <script type="text/javascript" src="js/Parsley.js-2.3.5/dist/i18n/pt-br.js"></script>
        <script type="text/javascript" src="js/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js"> </script>
        <script type="text/javascript" src="js/bootstrap-sweetalert-master/lib/sweet-alert.js"></script>
        
        <?php 

          if(isset($_SESSION['mensagem'])){ ?>
                       <script>
                           $(document).ready(function() {
                                swal("Sucesso", "Funcionario cadastrado com sucesso!", "success")
                           })
                        </script>
              
                      <?php unset($_SESSION['mensagem']);
                    }
          ?>
        
        <script> $(document).ready(function(){
            $('.date').mask('11/11/1111');
            $('.time').mask('00:00:00');
            $('.date_time').mask('00/00/0000 00:00:00');
            $('.cep').mask('00000-000');
            $('.phone').mask('0000-0000');
            $('.phone_with_ddd').mask('(00) 0000-0000');
            $('.phone_us').mask('(000) 000-0000');
            $('.mixed').mask('AAA 000-S0S');
            $('.cpf').mask('000.000.000-00', {reverse: true});
            $('.rg').mask('00.000.000-0');
            $('.money').mask('000.000.000.000.000,00', {reverse: true});
          }); 
        </script>
        <!-- Tratando telefones de São Paulo -->
        <script> 
            var maskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
                options = {onKeyPress: function(val, e, field, options) {
                    field.mask(maskBehavior.apply({}, arguments), options);
         }
      };
            $('.phone').mask(maskBehavior, options); 
        </script>
        
         <script type="text/javascript">
            $('#form').parsley();
        </script>

    </body>
</html>