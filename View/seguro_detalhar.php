<?php
    $idSeguro = $_GET["id"];
    require_once ("../Controller/crudPagamento.php");
    include("mpdf60/mpdf.php");

      $nome = $row['nomeCliente'];
      $cpf = $row['cpfCliente']; 
      $rg = $row['rgCliente'];
      $sexo = $row['sexoCliente'];
      $dataNascimento = $row['dataNascimento'];
      $idade = $row['idade'];
      $profissao = $row['profissao']; 
      $estadocivil = $row['estadocivil']; 
      $marca = $row['marca'];
      $modelo = $row['modelo'];
      $ano = $row['ano'];
      $placa = $row['placa'];
      $numeroPortas = $row['numeroPortas'];
      $usoVeiculo = $row['usoVeiculo'];
      $garagem = $row['garagem'];
      $seguranca = $row['seguranca'];
      $idSeguroCarro = $row['idSeguroCarro'];

    if (isset($_GET['btImprimir'])) {
      $html = "
           <fieldset>
          <div class='text-center'>
            <img src='img/logorelatorio.png' >
          </div>
          <br>
        <h3>Dados do Seguro:<h3> 
        <table class='table table-bordered'>
            <tbody>
              <tr>
                <th>Nome Cliente</th>
                  <td>$nome </td>
              </tr>
              <tr>
                <th>CPF</th>
                  <td>$cpf </td>
              </tr>

              <tr>
                <th>RG</th>
                  <td> $rg </td>
              </tr>
              <tr>
                <th>Sexo</th>
                  <td> $sexo</td>
              </tr>
              <tr>
                <th>Data Nascimento</th>
                  <td> $dataNascimento</td>
              </tr>
              <tr>
                <th>Idade</th>
                  <td> $idade</td>
              </tr>
              <tr>
                  <th>Profissão</th>
                    <td> $profissao</td>
                </tr>
                <tr>
                  <th>Estado Civil</th>
                    <td>  $estadocivil </td>
                </tr>            
               <tr>
                <th>Marca</th>
                  <td>$marca</td>
              </tr>
              <tr>
                  <th>Modelo</th>
                    <td> $modelo</td>
                </tr>
                <tr>
                  <th>Ano</th>
                    <td>  $ano </td>
                </tr>
                <tr>
                  <th>Placa</th>
                    <td> $placa</td>
                </tr>
                <tr>
                  <th>Numero de Portas</th>
                    <td> $numeroPortas </td>
                </tr>
                <tr>
                  <th>Uso Veiculo</th>
                    <td>$usoVeiculo </td>
                </tr>
                <tr>
                  <th>Garagem</th>
                    <td> $garagem </td>
                </tr>
                <tr>
                  <th>Segurança</th>
                    <td> $seguranca </td>
                </tr>
                <tr>
          </tbody>
        </table>       
           </fieldset>";

    $mpdf=new mPDF(); 
    $mpdf->SetDisplayMode('fullpage');
    $css = file_get_contents("css/cavalaro.css");
    $css = file_get_contents("css/bootstrap-3.3.6-dist/css/bootstrap.css");
    $mpdf->WriteHTML($css,1);
    $mpdf->WriteHTML($html);
    $mpdf->Output("seguro.pdf",'D');
}
?>

</html>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <script src="js/jquery-1.12.0.min.js"></script>    
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/bootstrap-sweetalert-master/lib/sweet-alert.js"></script>
        <link href="css/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/cavalaro.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/table.css" type="text/css" media="all" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="js/bootstrap-sweetalert-master/lib/sweet-alert.css">
        
        <title>Cavalaro - Home</title>
    </head>
    <body class="bgMain">
      <header>
        <?php include 'header.php';?>
      </header>

      <div class="container">
        <div class="pull-left">
            <a href="pagamento.php" id="desaparecer">
               <i class="fa fa-arrow-circle-left fa-3x " aria-hidden="true" title="Voltar" ></i>
              </a>
              <br><br> 
              <form>
                 <a href="seguro_detalhar.php?id=<?php echo $idSeguro ?>&btImprimir">
                   <i class="fa fa-print fa-3x" aria-hidden="true" title="Imprimir"></i>
                </a>
              </form> 
         </div> 
      <div class="row">
      <div class="col-md-4">
        <table>
            <tbody>
              <tr>
                <th>Nome Cliente</th>
                  <td><?php echo $nome ?></td>
              </tr>
              <tr>
                <th>CPF</th>
                  <td><?php echo $cpf ?></td>
              </tr>

              <tr>
                <th>RG</th>
                  <td> <?php echo $rg ?></td>
              </tr>
              <tr>
                <th>Sexo</th>
                  <td> <?php echo $sexo ?></td>
              </tr>
              <tr>
                <th>Data Nascimento</th>
                  <td><?php echo $dataNascimento ?></td>
              </tr>
              <tr>
                <th>Idade</th>
                  <td><?php echo $idade ?></td>
              </tr>
              <tr>
                  <th>Profissão</th>
                    <td> <?php echo $profissao ?></td>
                </tr>
                <tr>
                  <th>Estado Civil</th>
                    <td><?php echo $estadocivil ?></td>
                </tr>
          </tbody>
        </table>
        </div>
        <div class="col-md-4">
        <table >
            <tbody>            
               <tr>
                <th>Marca</th>
                  <td><?php echo $marca ?></td>
              </tr>
              <tr>
                  <th>Modelo</th>
                    <td> <?php echo $modelo ?></td>
                </tr>
                <tr>
                  <th>Ano</th>
                    <td><?php echo $ano ?></td>
                </tr>
                <tr>
                  <th>Placa</th>
                    <td><?php echo $placa ?></td>
                </tr>
                <tr>
                  <th>Numero de Portas</th>
                    <td><?php echo $numeroPortas ?></td>
                </tr>
                <tr>
                  <th>Uso Veiculo</th>
                    <td><?php echo $usoVeiculo ?></td>
                </tr>
                <tr>
                  <th>Garagem</th>
                    <td><?php echo $garagem ?></td>
                </tr>
                <tr>
                  <th>Segurança</th>
                    <td><?php echo $seguranca ?></td>
                </tr>
                <tr>
          </tbody>
        </table>
        </div>
     
      <div class="row"  id="desaparecer">
        <div class="col-md-3 btn-group">
        
          <h4 style="color:white;">Forma de pagamento:</h4>
          <button class="btn btn-warning" type="button" id="cartao">Cartão</button>     
          <button class="btn btn-warning" type="button" id="dinheiro">Dinheiro</button>
          <button class="btn btn-warning" type="button" id="boleto">Boleto</button>
        </div>

        <div id="pagcartao" style="display:none">
            <div class="col-md-3">
              <div class="row"> 
              <form  role="form" method="post">
                <div >
                  <h4 style="color:white;">Valor total: R$ <?php echo $row['valor']?></h4>
                  <input type="number" name="valor" value="<?php echo $row['valor']?>" hidden>
                  <input type="text" value="cartao" name="tipoPagamento" hidden>
                  <input type="number" name="idClienteFisico" value="<?php echo $id ?>" hidden>
                </div> 
                <div class="form-group">        
                  <select class="form-control" name="parcelado" required>
                      <option value="" hidden selected disabled>Parcelado em:</option>
                      <option value="">01</option>
                      <option value="">02</option>
                  </select>             
                </div> 
              </div>
              <div class="row">
                <div class="form-group">
                  <input type="submit" value="Pagar" name="btnPagar" class="btn btn-primary">
                </div>
              </div>
            </div>
          </form>
        </div>

        <div id="pagdinheiro" style="display: none" class="pdbot">
           <div class="col-md-3">
            <div class="row">
              <form role="form" method="post" action="../View/seguro_detalhar.php?id=<?php echo $idSeguro;?>">
              
                <h4 style="color:white;">Valor total: R$ <?php echo $row['valor']?></h4>

                <input type="number" name="valor" value="<?php echo $row['valor']?>" hidden>
                <input type="text" name="tipoPagamento" value="dinheiro" hidden>
                <input type="number" name="idFuncionario" value="<?php echo $_SESSION['idFuncionario']; ?>" hidden>

                <input type="number" name="idCliente" value="<?php echo $id; ?>" hidden>
                <input type="number" name="idSeguro" value="<?php echo $idSeguroCarro; ?>" hidden>

                <input type="submit" value="Pagar" name="btnPagar" class="btn btn-primary">
               
               </form>
              </div>
            </div>
           </div>
           
       
        </div>
        </div>
        </div>
        </form>
        </div>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#cartao").click(cartao);
                $("#dinheiro").click(dinheiro);
                });
 
            function cartao(){
                $("#pagcartao").toggle();
            }
            function dinheiro(){
              $("#pagdinheiro").toggle();
            }

          <?php
          if ($perm == 'corretor') { ?>
              $('#cartao').attr('disabled', true);
              $('#dinheiro').attr('disabled', true);
              $('#boleto').attr('disabled', true);  
              $('#desaparecer').attr('hidden', true);                 
      <?php } ?>

        </script>
    </body>    
</html>
