<?php
    require_once ("../Controller/crudPagamentoContas.php");
?>

<!DOCTYPE html>

    <html>
    <head>
        <meta charset="utf-8">
        
        <link href="css/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" media="all" rel="stylesheet">
        <link href="css/cavalaro.css" type="text/css" media="all" rel="stylesheet">
        
        <title>Cavalaro - Pagamento de Contas</title>
    </head>

    <body class="bgMain">
        <header>
          <?php include 'header.php';?>
        </header>

            <div class="container">
            <form method="post" action="../View/pagamentoContas.php">
                <div class="col-md-3">
                    <input type="text" name="descricao" placeholder="Descrição" class="form-control input-sm input text-center" required="">
                </div>
                <div class="col-md-3">
                    <input type="text" name="valor" placeholder="Valor" class="form-control input-sm input text-center" required="">
                </div>
                <div class="col-md-2">
                    <input type="submit" name="btnCadastrar" value="Cadastrar" class="btn btn-info btn-lg btn-primary btAlterarCadastrar" />
                </div>
            </form>
            <form method="post" action="../Controller/crudPagamentoContas.<?php  ?>">
                <div class="col-md-1" hidden>
                    <input type="submit" name="btnPagar" value="Pagar" class="btn btn-info btn-lg btn-primary btAlterarCadastrar" />
                </div>
            </form>

              <div class="col-md-2" style="text-align: right;">
                    <a href="pagamentoContas_relatorio.php">
                        <input type="submit" name="btnGerarRelatorio" value="Relatorios" class="btn btn-info btn-lg btn-danger btExcliuir" />
                    </a>
                </div>
            </div>
           
            <div class="container">
                <div class="panel-body">
                <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">Descrição do pagamento</th>
                        <th class="text-center">Valor</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td align="right">Valor Total</td>
                        <td align="center">$ <?php echo $sum; ?></td>
                    </tr>
                </tfoot>
                    <?php foreach($dados as $row){ ?>

                    <tr>
                        <td align="center"><?php echo $row['descricaoPagamento']?></td>
                        <td align="center"><?php echo $row['valorPagamento'] ?></td>
                    </tr>

                    <?php } ?>

                </table>
                </div>
            </div>
        
        <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>        
    </body>
</html>