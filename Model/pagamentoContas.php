<?php

	class pagamentoContas{
		private $descricao;
		private $valor;
		private $valorTotal;
		private $dataPagamento;

	public function getDescricao(){
		return $this->descricao;
	}

	public function setDescricao($descricao){
		$this->descricao = $descricao;
	}

	public function getValor(){
		return $this->valor;
	}
	public function setValor($valor){
		$this->valor = $valor;
	}

	public function getValorTotal(){
		return $this->valorTotal;
	}
	public function setValorTotal($valorTotal){
		$this->valorTotal = $valorTotal;
	}
	public function getDataPagamento(){
		return $this->dataPagamento;
	}
	public function setDataPagamento($dataPagamento){
		$this->dataPagamento = $dataPagamento;
	}
}
?>