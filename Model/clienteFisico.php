<?php

class clienteFisico{
            private $idCliente;
            private $nomeCliente;
            private $cpfCliente;
            private $rgCliente;
            private $sexoCliente;
            private $dataNascimento;
            private $idade;
            private $profissao;
            private $estadoCivil;
            private $cep;
            private $endereco;
            private $bairro;
            private $cnhPessoa;
            private $uf;
            private $telefone;
            private $email;
            private $cidade;
    
            function getIdCliente() {
                return $this->idCliente;
            }

            function getNomeCliente() {
                return $this->nomeCliente;
            }

            function getCpfCliente() {
                return $this->cpfCliente;
            }
    
            function getRgCliente() {
                return $this->rgCliente;
            }

            function getSexoCliente() {
                return $this->sexoCliente;
            }

            function getDataNascimento() {
                return $this->dataNascimento;
            }

            function getIdade() {
                return $this->idade;
            }

            function getProfissao() {
                return $this->profissao;
            }
            function getEstadoCivil() {
                return $this->estadoCivil;
            }
            
             public function getCep(){
              return $this->cep;
            }

            public function getEndereco(){
              return $this->endereco;
            }

            public function getBairro(){
              return $this->bairro;
            }
            
            public function getCnhPessoa(){
                return $this->cnhPessoa;
            }

            public function getUf(){
              return $this->uf;
            }
    
            public function getTelefone(){
              return $this->telefone;
            }

            public function getEmail(){
              return $this->email;
            } 

            public function getCidade(){
          		return $this->cidade;
          	}

            function setIdCliente($idCliente) {
                $this->idCliente = $idCliente;
            }

            function setNomeCliente($nomeCliente) {
                $this->nomeCliente = $nomeCliente;
            }

            function setCpfCliente($cpfCliente) {
                $this->cpfCliente = $cpfCliente;
            }
            
            function setRgCliente($rgCliente) {
                $this->rgCliente = $rgCliente;
            }

            function setSexoCliente($sexoCliente) {
                $this->sexoCliente = $sexoCliente;
            }

            function setDataNascimento($dataNascimento) {
                $this->dataNascimento = $dataNascimento;
            }

            function setIdade($idade) {
                $this->idade = $idade;
            }
    
            function setProfissao($profissao) {
                $this->profissao = $profissao;
            }
            
            function setEstadoCivil($estadoCivil) {
                $this->estadoCivil = $estadoCivil;
            }
    
            public function setCep($cep){
              $this->cep = $cep;
            }
            
            public function setEndereco($endereco){
              $this->endereco = $endereco;
            }
    
            public function setBairro($bairro){
              $this->bairro = $bairro;
            }
    
            public function setCnhPessoa($cnhPessoa){
                $this->cnhPessoa = $cnhPessoa;
            }
            
            public function setUf($uf){
              $this->uf = $uf;
            }
            
            public function setTelefone($telefone){
              $this->telefone = $telefone;
            }
    
            public function setEmail($email){
              $this->email = $email;
            }
    
            public function setCidade($cidade){
          		$this->cidade = $cidade;
          	}
}
    
?>