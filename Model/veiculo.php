<?php 
    class seguroVeiculo{
        private $idSeguroCarro;
        private $marca;
        private $modelo;
        private $combutivelCarro;
        private $ano;
        private $placa;
        private $numeroPortas;
        private $usoVeiculo;
        private $garagem;
        private $seguranca;
        private $possuiPortao;
        private $tipoPagamento;
        private $valor;
        private $data;


    public function getIdSeguroCarro(){
        return $this->idSeguroCarro;
    }

    public function setIdSeguroCarro($idSeguroCarro){
        $this->idSeguroCarro = $idSeguroCarro;
    }

    public function getMarca(){
        return $this->marca;
    }

    public function setMarca($marca){
        $this->marca = $marca;
    }

    public function getModelo(){
        return $this->modelo;
    }

    public function setModelo($modelo){
        $this->modelo = $modelo;
    }
    public function getCombustivelCarro(){
        return $this->combustivelCarro;
    }

    public function setCombustivelCarro($combustivelCarro){
        $this->combustivelCarro = $combustivelCarro;
    }

    public function getAno(){
        return $this->ano;
    }

    public function setAno($ano){
        $this->ano = $ano;
    }

    public function getPlaca(){
        return $this->placa;
    }

    public function setPlaca($placa){
        $this->placa = $placa;
    }

    public function getNumeroPortas(){
        return $this->numeroPortas;
    }

    public function setNumeroPortas($numeroPortas){
        $this->numeroPortas = $numeroPortas;
    }

    public function getUsoVeiculo(){
        return $this->usoVeiculo;
    }

    public function setUsoVeiculo($usoVeiculo){
        $this->usoVeiculo = $usoVeiculo;
    }

    public function getGaragem(){
        return $this->garagem;
    }

    public function setGaragem($garagem){
        $this->garagem = $garagem;
    }

    public function getSeguranca(){
        return $this->seguranca;
    }

    public function setSeguranca($seguranca){
        $this->seguranca = $seguranca;
    }

    public function getPossuiPortao(){
        return $this->possuiPortao;
    }

    public function setPossuiPortao($possuiPortao){
        $this->possuiPortao = $possuiPortao;
    }
    public function getTipoPagamento(){
        return $this->tipoPagamento;
    }

    public function setTipoPagamento($tipoPagamento){
        $this->tipoPagamento = $tipoPagamento;
    }

    public function getValor(){
        return $this->valor;
    }

    public function setValor($valor){
        $this->valor = $valor;
    }
    public function getData(){
        return $this->data;
    }

    public function setData($data){
        $this->data = $data;
    }

    }
?>