<?php 

class seguradora{

	private $nomeSeguradora;
	private $cnpj;
	private $razaoSocial;
	private $telefoneSeguradora;
	private $homepage;
	private $representante;
	private $emailSeguradora;
	private $logradouraSeguradora;
	private $bairroSeguradora;
	private $cidadeSeguradora;
	private $cepSeguradora;
	private $ufSeguradora;

	public function getNomeSeguradora(){
		return $this->nomeSeguradora;
	}

	public function setNomeSeguradora($nomeSeguradora){
		$this->nomeSeguradora = $nomeSeguradora;
	}

	public function getCnpj(){
		return $this->cnpj;
	}

	public function setCnpj($cnpj){
		$this->cnpj = $cnpj;
	}

	public function getRazaoSocial(){
		return $this->razaoSocial;
	}

	public function setRazaoSocial($razaoSocial){
		$this->razaoSocial = $razaoSocial;
	}

	public function getTelefoneSeguradora(){
		return $this->telefoneSeguradora;
	}

	public function setTelefoneSeguradora($telefoneSeguradora){
		$this->telefoneSeguradora = $telefoneSeguradora;
	}

	public function getHomepage(){
		return $this->homepage;
	}

	public function setHomepage($homepage){
		$this->homepage = $homepage;
	}

	public function getRepresentante(){
		return $this->representante;
	}

	public function setRepresentante($representante){
		$this->representante = $representante;
	}

	public function getEmailSeguradora(){
		return $this->emailSeguradora;
	}

	public function setEmailSeguradora($emailSeguradora){
		$this->emailSeguradora = $emailSeguradora;
	}

	public function getLogradouraSeguradora(){
		return $this->logradouraSeguradora;
	}

	public function setLogradouraSeguradora($logradouraSeguradora){
		$this->logradouraSeguradora = $logradouraSeguradora;
	}

	public function getBairroSeguradora(){
		return $this->bairroSeguradora;
	}

	public function setBairroSeguradora($bairroSeguradora){
		$this->bairroSeguradora = $bairroSeguradora;
	}

	public function getCidadeSeguradora(){
		return $this->cidadeSeguradora;
	}

	public function setCidadeSeguradora($cidadeSeguradora){
		$this->cidadeSeguradora = $cidadeSeguradora;
	}

	public function getCepSeguradora(){
		return $this->cepSeguradora;
	}

	public function setCepSeguradora($cepSeguradora){
		$this->cepSeguradora = $cepSeguradora;
	}

	public function getUfSeguradora(){
		return $this->ufSeguradora;
	}

	public function setUfSeguradora($ufSeguradora){
		$this->ufSeguradora = $ufSeguradora;
	}
}

 ?>