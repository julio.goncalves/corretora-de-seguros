<?php
class funcionario{
            private $nomeFuncionario;
            private $cpfFuncionario;
            private $sexoFuncionario;
            private $salario;
            private $senha;
            private $permissao;
            private $rgFuncionario;
            private $dataNasc;
            private $cep;
            private $endereco;
            private $bairro;
            private $uf;
            private $telefone;
            private $email;
            private $cidade;

            public function getNomeFuncionario(){
              return $this->nomeFuncionario;
            }

            public function setNomeFuncionario($nomeFuncionario){
              $this->nomeFuncionario = $nomeFuncionario;
            }

            public function getCpfFuncionario(){
              return $this->cpfFuncionario;
            }

            public function setCpfFuncionario($cpfFuncionario){
              $this->cpfFuncionario = $cpfFuncionario;
            }

            public function getSexoFuncionario(){
              return $this->sexoFuncionario;
            }

            public function setSexoFuncionario($sexoFuncionario){
              $this->sexoFuncionario = $sexoFuncionario;
            }

            public function getSalario(){
              return $this->salario;
            }

            public function setSalario($salario){
              $this->salario = $salario;
            }

            public function getSenha(){
              return $this->senha;
            }

            public function setSenha($senha){
              $this->senha = $senha;
            }

            public function getPermissao(){
              return $this->permissao;
            }

            public function setPermissao($permissao){
              $this->permissao = $permissao;
            }

            public function getRgFuncionario(){
              return $this->rgFuncionario;
            }

            public function setRgFuncionario($rgFuncionario){
              $this->rgFuncionario = $rgFuncionario;
            }

            public function getDataNasc(){
              return $this->dataNasc;
            }

            public function setDataNasc($dataNasc){
              $this->dataNasc = $dataNasc;
            }

            public function getCep(){
              return $this->cep;
            }

            public function setCep($cep){
              $this->cep = $cep;
            }

            public function getEndereco(){
              return $this->endereco;
            }

            public function setEndereco($endereco){
              $this->endereco = $endereco;
            }

            public function getBairro(){
              return $this->bairro;
            }

            public function setBairro($bairro){
              $this->bairro = $bairro;
            }

            public function getUf(){
              return $this->uf;
            }

            public function setUf($uf){
              $this->uf = $uf;
            }

            public function getTelefone(){
              return $this->telefone;
            }

            public function setTelefone($telefone){
              $this->telefone = $telefone;
            }

            public function getEmail(){
              return $this->email;
            }

            public function setEmail($email){
              $this->email = $email;
            }

            public function getCidade(){
          		return $this->cidade;
          	}

          	public function setCidade($cidade){
          		$this->cidade = $cidade;
          	}

}
?>
