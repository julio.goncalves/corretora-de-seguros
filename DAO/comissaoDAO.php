<?php

    require_once("conexao.php");

class comissaoDAO {

    //construtor
    function comissaoDAO(){
        $this->con = new Conexao();
        $this->pdo = $this->con->Connect();
    }

    function consultarAll(){
    	try{
            $stmt = $this->pdo->prepare("SELECT * FROM funcionario INNER JOIN seguroveiculo ON funcionario.idFuncionario = seguroveiculo.idFuncionario INNER JOIN comissao ON comissao.idSeguroCarro = seguroveiculo.idSeguroCarro WHERE month(dataComissao) = MONTH(NOW())");

            $stmt->execute();
            $consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $consulta;

        } catch (PDOException $ex) {
            echo "ERRO 02: {$ex->getMessage()}";
        }
    }

    function inserir($total, $idSeguroCarro){
        try{
            $stmt = $this->pdo->prepare("INSERT INTO comissao VALUES ('', :total, NOW(), :idSeguroCarro)");

            $param = array(
                ':total' => $total,
                ':idSeguroCarro' => $idSeguroCarro,
                );

            $stmt->execute($param);

            return true;

        }catch (PDOException $ex) {
            echo "ERRO 02: {$ex->getMessage()}";
        }
    }

    function soma(){
        try{
            $stmt = $this->pdo->prepare("SELECT valorCom FROM comissao");

            $stmt->execute();

            $consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $consulta;

        }catch (PDOException $ex){
            echo "ERRO 02: {$ex->getMessage()}";
        }
    }

    function filtrar($comissao){
        $data = $_POST['filtro'];
        try{
            $stmt = $this->pdo->prepare("SELECT * FROM funcionario INNER JOIN seguroveiculo ON funcionario.idFuncionario = seguroveiculo.idFuncionario INNER JOIN comissao ON comissao.idSeguroCarro = seguroveiculo.idSeguroCarro WHERE month(dataComissao) = ?");

            $stmt->bindValue( 1 , $data, PDO::PARAM_STR );
            $stmt->execute();

            $consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $consulta;

        }catch (PDOException $ex){
            echo "ERRO 02: {$ex->getMessage()}";
        }
    }

    function consultarbyId($id) {
        try{
            $stmt = $this->pdo->prepare("SELECT * FROM seguroveiculo INNER JOIN comissao ON comissao.idSeguroCarro = seguroveiculo.idSeguroCarro INNER JOIN clientefisico ON seguroveiculo.idCliente = clientefisico.idClienteFisico WHERE comissao.idComissao = :id");

            $stmt->bindParam( ':id' , $id , PDO::PARAM_STR );

            $stmt->execute();
            $consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $consulta;

        }catch (PDOException $ex){
            echo "ERRO 02: {$ex->getMessage()}";
        }
    }

}
