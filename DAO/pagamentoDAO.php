<?php

    require_once("conexao.php");

class pagamentoDAO {

    //construtor
    function pagamentoDAO(){
        $this->con = new Conexao();
        $this->pdo = $this->con->Connect();
    }

    function consultarAll(){
    	try{
            $stmt = $this->pdo->prepare("SELECT seguroveiculo.idSeguroCarro, seguroveiculo.tipoPagamento, seguroveiculo.valor, clientefisico.nomeCliente, clientefisico.idClienteFisico
             FROM clientefisico INNER JOIN seguroveiculo ON clientefisico.idClienteFisico = seguroveiculo.idCliente WHERE clientefisico.status = 'A'");

            $stmt->execute();
            $consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $consulta;

        } catch (PDOException $ex) {
            echo "ERRO 02: {$ex->getMessage()}";
        }
    }

    function consultarbyId($id) {
        try{
            $stmt = $this->pdo->prepare("SELECT * FROM clientefisico INNER JOIN seguroveiculo ON clientefisico.idClienteFisico = seguroveiculo.idCliente WHERE clientefisico.idClienteFisico = :id");

            $stmt->bindParam( ':id' , $id , PDO::PARAM_STR );

            $stmt->execute();
            $consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $consulta;

        }catch (PDOException $ex){
            echo "ERRO 02: {$ex->getMessage()}";
        }
    }

    function alterar($seguro, $idCliente){
        try{
            $stmt = $this->pdo->prepare("UPDATE seguroveiculo SET tipoPagamento = :tipopagamento, status = 'A' WHERE idCliente = :id");

            $param = array(
                ':tipopagamento' => $seguro,
                ':id' => $idCliente,
                );
            $stmt->execute($param);

            return true;
        }catch (PDOException $ex){
            echo "ERRO 02: {$ex->getMessage()}";
        }
    }

}
