<?php

	require_once("conexao.php");

class pagamentoContasDAO {

	function pagamentoContasDAO(){
		$this->con = new Conexao();
        $this->pdo = $this->con->Connect();
	}

	function cadastrar($pagamentoContas){
		try{
			 $stmt = $this->pdo->prepare("INSERT INTO pagamentocontas VALUES ('', :descricaoPagamento, :valorPagamento,:valorTotal, NOW())");

			 $param = array(
			 	':descricaoPagamento' => $pagamentoContas->getDescricao(),
			 	':valorPagamento' => $pagamentoContas->getValor(),
			 	':valorTotal' => $pagamentoContas->getValorTotal(),
			  );

			 $stmt->execute($param);

		}catch (PDOException $ex) {
            echo "ERRO 01: {$ex->getMessage()}";

        }
	}

	function consultarAll(){
		try{
			$stmt = $this->pdo->prepare("SELECT * FROM pagamentocontas WHERE month(dataPagamento) = MONTH(NOW())");

			$stmt->execute();

			$consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $consulta;

		}catch (PDOException $ex){
			echo "ERRO 02: {$ex->getMessage()}";
		}
	}

	function soma(){
		try{
			$stmt = $this->pdo->prepare("SELECT valorPagamento FROM pagamentocontas WHERE month(dataPagamento) = MONTH(NOW())");

			$stmt->execute();

			$consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);

			return $consulta;

		}catch (PDOException $ex){
			echo "ERRO 02: {$ex->getMessage()}";
		}
	}

	function cadastrarSoma($pagamentoContas){
		try{
			$stmt = $this->pdo->prepare("INSERT INTO pagamentocontas VALUES ('','','',:somaTotal)");

			$stmt->bindParam(':somaTotal',$pagamentoContas->getValorTotal(), PDO::PARAM_STR);

			$stmt->execute();
		}catch (PDOException $ex){
			echo "ERRO 02: {$ex->getMessage()}";
		}
	}

	function filtrar($pagamentoContas){
		$data = $_POST['filtro'];
		try{
			$stmt = $this->pdo->prepare("SELECT * from pagamentocontas where month(dataPagamento) = ?");

			$stmt->bindValue( 1 , $data, PDO::PARAM_STR );
			$stmt->execute();

			$consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $consulta;

		}catch (PDOException $ex){
			echo "ERRO 02: {$ex->getMessage()}";
		}
	}

}

?>
