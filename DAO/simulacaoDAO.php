<?php
    require_once("conexao.php");

class simulacaoDAO {

    //construtor
    function simulacaoDAO(){
        $this->con = new Conexao();
        $this->pdo = $this->con->Connect();
    }

    function consultarAll(){
    	try{
            $stmt = $this->pdo->prepare("SELECT seguroveiculo.idSeguroCarro, seguroveiculo.tipoPagamento, seguroveiculo.valor, seguroveiculo.status, clientefisico.nomeCliente, clientefisico.idClienteFisico
             FROM clientefisico INNER JOIN seguroveiculo ON clientefisico.idClienteFisico = seguroveiculo.idCliente WHERE clientefisico.status = 'A' AND seguroveiculo.status =	 'I'");

            $stmt->execute();
            $consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $consulta;

        } catch (PDOException $ex) {
            echo "ERRO 02: {$ex->getMessage()}";
        }
    }

    function consultarbyId($id) {
        try{
            $stmt = $this->pdo->prepare("SELECT * FROM pessoa INNER JOIN clientefisico on pessoa.idClienteFisico = clientefisico.idClienteFisico INNER JOIN seguroveiculo ON clientefisico.idClienteFisico = seguroveiculo.idCliente WHERE clientefisico.idClienteFisico = :id");

            $stmt->bindParam( ':id' , $id , PDO::PARAM_STR );

            $stmt->execute();
            $consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $consulta;

        }catch (PDOException $ex){
            echo "ERRO 02: {$ex->getMessage()}";
        }
    }

    function alterar ($veiculo, $idClienteFisico){
        try{
            $stmt = $this->pdo->prepare("UPDATE seguroveiculo SET valor = :valor WHERE idCliente = :idCliente");
            $param = array(
                    ":valor" => $veiculo,
                    ":idCliente" => $idClienteFisico,
            );
            $stmt->execute($param);

            return true;

    }catch (PDOException $ex){
            echo "ERRO 02: {$ex->getMessage()}";
        }
    }
}

?>
