<?php

require_once("conexao.php");

class seguradoraDAO{

	function seguradoraDAO(){
		$this->con = new Conexao();
        $this->pdo = $this->con->Connect();
	}

	function cadastrar($seguradora){
		try{

			$stmt = $this->pdo->prepare("INSERT INTO seguradora VALUES ('',:nomeSeguradora, :cnpj, :razaoSocial, :telefoneSeguradora, :homepage, :representante, :emailSeguradora, :logradouroSeguradora, :bairroSeguradora, :cidadeSeguradora, :cepSeguradora, :ufSeguradora, 'A', NOW())");
			$param = array(
				':nomeSeguradora' => $seguradora->getNomeSeguradora(),
				':cnpj' => $seguradora->getCnpj(),
				':razaoSocial' => $seguradora->getRazaoSocial(),
				':telefoneSeguradora' => $seguradora->getTelefoneSeguradora(),
				':homepage' => $seguradora->getHomepage(),
				':representante' => $seguradora->getRepresentante(),
				':emailSeguradora' => $seguradora->getEmailSeguradora(),
				':logradouroSeguradora' => $seguradora->getLogradouraSeguradora(),
				':bairroSeguradora' => $seguradora->getBairroSeguradora(),
				':cidadeSeguradora' => $seguradora->getCidadeSeguradora(),
				':cepSeguradora' => $seguradora->getCepSeguradora(),
				'ufSeguradora' => $seguradora->getUfSeguradora(),
				);
			$stmt->execute($param);

			return true;

		}catch (PDOException $ex) {
            echo "ERRO 01: {$ex->getMessage()}";

        }
	}

	function consultar ($seguradora){
		$nomeSeguradora = $_POST['nomeSeguradora'];
		try{
			$stmt = $this->pdo->prepare("SELECT * FROM seguradora WHERE nomeSeguradora LIKE ? AND status = 'A'");

			$stmt->bindValue( 1 , "%$nomeSeguradora%", PDO::PARAM_STR );

			$stmt->execute();

			$consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $consulta;

		}catch (PDOException $ex) {
            echo "ERRO 02: {$ex->getMessage()}";
        }
	}

	function consultarAll($pag){
		$qtdlimite = 10;
        $apartirde = $pag*$qtdlimite;
		try{
			$stmt = $this->pdo->prepare("SELECT * FROM seguradora WHERE status = 'A' ORDER BY idSeguradora DESC LIMIT $apartirde, $qtdlimite ");

			$stmt->execute();

			$consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $consulta;

		}catch (PDOException $ex){
			echo "ERRO 02: {$ex->getMessage()}";
		}
	}

 	 function numeroLinhas(){
	    $limite = 10;
	    $sql_total = 'SELECT * FROM seguradora';
	    try{
	        $query_Total = $this->pdo->prepare($sql_total);
	        $query_Total->execute();

	        $query_result = $query_Total->fetchAll(PDO::FETCH_ASSOC);
	        $query_count =  $query_Total->rowCount(PDO::FETCH_ASSOC);

	        return $query_count;

	    } catch(PDOException $ex){
	        echo "ERRO: {$ex->getMessage()}";
	    }
	}

	function consultarbyId($id) {
		try{
			$stmt = $this->pdo->prepare("SELECT * FROM seguradora WHERE idSeguradora = :id AND status = 'A'");

			$stmt->bindParam( ':id' , $id , PDO::PARAM_STR );

			$stmt->execute();
			$consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $consulta;

		}catch (PDOException $ex){
			echo "ERRO 02: {$ex->getMessage()}";
		}
	}

	function alterar($seguradora, $idSeguradora){
		try{
			$stmt = $this->pdo->prepare("UPDATE seguradora SET nomeSeguradora = :nome, cnpj = :cnpj, razaoSocial = :razaoSocial, telefoneSeguradora = :telefone, homepage = :homepage, representante = :representante, emailSeguradora = :email, logradouroSeguradora = :logradouro, bairroSeguradora = :bairro, cidadeSeguradora = :cidade, cepSeguradora = :cep, ufSeguradora = :uf WHERE idSeguradora = :id ");

			$param = array(
				':nome' => $seguradora->getNomeSeguradora(),
				':cnpj' => $seguradora->getCnpj(),
				':razaoSocial' => $seguradora->getRazaoSocial(),
				':telefone' => $seguradora->getTelefoneSeguradora(),
				':homepage' => $seguradora->getHomepage(),
				':representante' => $seguradora->getRepresentante(),
				':email' => $seguradora->getEmailSeguradora(),
				':logradouro' => $seguradora->getLogradouraSeguradora(),
				':bairro' => $seguradora->getBairroSeguradora(),
				':cidade' => $seguradora->getCidadeSeguradora(),
				':cep' => $seguradora->getCepSeguradora(),
				'uf' => $seguradora->getUfSeguradora(),
				':id'=> $idSeguradora,
				);

			$stmt->execute($param);

			return true;

		}catch (PDOException $ex){
			echo "ERRO 02: {$ex->getMessage()}";
		}

	}

	function deletar($id){
		 try{
             $stmt = $this->pdo->prepare("UPDATE seguradora SET status = 'I' WHERE idSeguradora = :id ");
            $stmt->bindParam(':id' , $id , PDO::PARAM_STR );
            $stmt->execute();

            return true;

        }catch(PDOException $ex){
            echo "ERRO: {$ex->getMessage()}";
            return false;
        }
	}

	function filtrar($seguradora){
		$data = $_POST['filtro'];
		try{
			$stmt = $this->pdo->prepare("SELECT * from seguradora where month(data) = ?");

			$stmt->bindValue( 1 , $data, PDO::PARAM_STR );
			$stmt->execute();

			$consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $consulta;

		}catch (PDOException $ex){
			echo "ERRO 02: {$ex->getMessage()}";
		}
	}
}

?>
