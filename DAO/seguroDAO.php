<?php
	require_once("conexao.php");

	class seguroDAO{

		function seguroDAO(){
			$this->con = new Conexao();
        	$this->pdo = $this->con->Connect();
		}

		function cadastrarSeguro($clienteFisico, $veiculo,$idFuncionario, $image){

            try {

            $stmt = $this->pdo->prepare("INSERT INTO clientefisico VALUES ('', :nome, :cpf, :rg, :sexo, :datanasc, :idade, :profissao, :estadocivil, 'A', :image)");

            $param = array(
                ":nome" => $clienteFisico->getNomeCliente(),
                ":cpf" => $clienteFisico->getCpfCliente(),
                ":rg" => $clienteFisico->getRgCliente(),
                ":sexo" => $clienteFisico->getSexoCliente(),
                ":datanasc" => $clienteFisico->getDataNascimento(),
                ":idade" => $clienteFisico->getIdade(),
                ":profissao" => $clienteFisico->getProfissao(),
                ":estadocivil" => $clienteFisico->getEstadoCivil(),
                ":image" => $image
            );

            $stmt->execute($param);
            $id =  $this->pdo->lastInsertId('idClienteFisico');
            $stmt = $this->pdo->prepare("INSERT INTO pessoa VALUES ('',LAST_INSERT_ID(),'',:endereco, :bairro, :cidade, :cep, :email, :telefone, :cnh, :uf,'',NOW())");

            $param = array(
                ":endereco" => $clienteFisico->getEndereco(),
                ":bairro" => $clienteFisico->getBairro(),
                ":cidade" => $clienteFisico->getCidade(),
                ":cep" => $clienteFisico->getCep(),
                ":email" => $clienteFisico->getEmail(),
                ":telefone" => $clienteFisico->getTelefone(),
                ":cnh" => $clienteFisico->getCnhPessoa(),
                ":uf" => $clienteFisico->getUf(),
            );

            $stmt->execute($param);

            $stmt = $this->pdo->prepare("INSERT INTO seguroveiculo VALUES ('', $id,:fabricanteCarro, :modeloCarro,:combustivel, :anoCarro, :placaCarro, :nportasCarro, :usoCarro, :GaragemCarro, :seguranca, :possuiPortao,'em aberto','',:data, 'I', :idFuncionario )");
            $param = array(
                ":fabricanteCarro" => $veiculo->getMarca(),
                ":modeloCarro" => $veiculo->getModelo(),
                ":combustivel" =>$veiculo->getCombustivelCarro(),
                ":anoCarro" => $veiculo->getAno(),
                ":placaCarro" => $veiculo->getPlaca(),
                ":nportasCarro" => $veiculo->getNumeroPortas(),
                ":usoCarro" => $veiculo->getUsoVeiculo(),
                ":GaragemCarro" => $veiculo->getGaragem(),
                ":seguranca" => $veiculo->getSeguranca(),
                ":possuiPortao" => $veiculo->getPossuiPortao(),
                ":data" => $veiculo->getData(),
                ":idFuncionario" => $idFuncionario,
            );

            $stmt->execute($param);
            return true;

        } catch (PDOException $ex) {
            echo "ERRO 01: {$ex->getMessage()}";

        }
	}

	}
?>
