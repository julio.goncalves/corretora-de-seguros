    <?php

    require_once("conexao.php");

class funcionarioDAO {

    //construtor
    function funcionarioDAO(){
        $this->con = new Conexao();
        $this->pdo = $this->con->Connect();
    }

    function cadastrar($funcionario) {
        try {
            $stmt = $this->pdo->prepare("INSERT INTO funcionario VALUES ('', :cpfFuncionario, :sexoFuncionario, :salario,:nomeFuncionario,:senha, :permissao, :rgFuncionario, '', '', '', 'A',:data)");
            $param = array(
                ":cpfFuncionario" => $funcionario->getCpfFuncionario(),
                ":sexoFuncionario" => $funcionario->getSexoFuncionario(),
                ":salario" => $funcionario->getSalario(),
                ":nomeFuncionario" => $funcionario->getNomeFuncionario(),
                ":senha" => $funcionario->getSenha(),
                ":permissao" => $funcionario->getPermissao(),
                ":rgFuncionario" => $funcionario->getRgFuncionario(),
                ":data" => $funcionario->getDataNasc(),
            );

            $stmt->execute($param);

            $stmt2 = $this->pdo->prepare("INSERT INTO pessoa VALUES ('','','',:enderecoPessoa, :bairroPessoa, :cidadePessoa, :cepPessoa,:emailPessoa, :telefonePessoa,'0', :ufPessoa, LAST_INSERT_ID(), NOW())");
            $param = array(
                ":enderecoPessoa" => $funcionario->getEndereco(),
                ":bairroPessoa" => $funcionario->getBairro(),
                ":cidadePessoa" => $funcionario->getCidade(),
                ":cepPessoa" => $funcionario->getCep(),
                ":emailPessoa" => $funcionario->getEmail(),
                ":telefonePessoa" => $funcionario->getTelefone(),
                ":ufPessoa" => $funcionario->getUf(),
            );

           $stmt2->execute($param);

           return true;

        } catch (PDOException $ex) {
            echo "ERRO 01: {$ex->getMessage()}";
            return false;

        }finally{
            // close connection;
        }
    }

      function consultar($funcionario) {
        $nome = $_POST['nome'];
        try{
            $stmt = $this->pdo->prepare("SELECT * FROM funcionario WHERE nomeFuncionario LIKE ? AND status = 'A' ORDER BY idFuncionario DESC ");

            $stmt->bindValue( 1 , "%$nome%", PDO::PARAM_STR );

            $stmt->execute();
            $consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $consulta;
        } catch (PDOException $ex) {
            echo "ERRO 02: {$ex->getMessage()}";
        }
    }

   function consultarAll($pag) {
        $qtdlimite = 10;
        $apartirde = $pag*$qtdlimite;
        try{
            $stmt = $this->pdo->prepare("SELECT * FROM funcionario WHERE status = 'A' ORDER BY idFuncionario DESC LIMIT $apartirde, $qtdlimite ");

            $stmt->execute();

            $consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $consulta;

        } catch (PDOException $ex) {
            echo "ERRO 02: {$ex->getMessage()}";
        }
    }

    function numeroLinhas(){
        $limite = 10;
        $sql_total = 'SELECT * FROM funcionario';
        try{
            $query_Total = $this->pdo->prepare($sql_total);
            $query_Total->execute();

            $query_result = $query_Total->fetchAll(PDO::FETCH_ASSOC);
            $query_count =  $query_Total->rowCount(PDO::FETCH_ASSOC);

            $qtdPag = ceil($query_count/$limite);

            return $query_count;

        } catch(PDOException $ex){
            echo "ERRO: {$ex->getMessage()}";
        }
    }

    function Deletar($id){
        try{
             $stmt = $this->pdo->prepare("UPDATE funcionario SET status = 'I' WHERE idFuncionario = :id ");
            $stmt->bindParam(':id' , $id , PDO::PARAM_STR );
            $stmt->execute();

            return true;

        }catch(PDOException $ex){
            echo "ERRO: {$ex->getMessage()}";
            return false;
        }
    }

    function consultarbyId($id) {

        try{
            $stmt = $this->pdo->prepare("SELECT pessoa.enderecoPessoa, pessoa.bairroPessoa, pessoa.cidadePessoa, pessoa.cepPessoa, pessoa.emailPessoa, pessoa.telefonePessoa, pessoa.ufPessoa, funcionario.cpfFuncionario, funcionario.sexoFuncionario,funcionario.salario,funcionario.nomeFuncionario, funcionario.senha, funcionario.permissao, funcionario.rgFuncionario, funcionario.dataNascimento FROM pessoa INNER JOIN funcionario ON pessoa.idFuncionario = funcionario.idFuncionario WHERE funcionario.idFuncionario = :id  ");

            $stmt->bindParam( ':id' , $id , PDO::PARAM_STR );

            $stmt->execute();
            $consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $consulta;

        } catch (PDOException $ex) {
            echo "ERRO 02: {$ex->getMessage()}";
        }
    }

    function alterar($funcionario, $idFuncionario){
        try{
            $stmt = $this->pdo->prepare("UPDATE funcionario SET cpfFuncionario = :cpfFuncionario, sexoFuncionario = :sexoFuncionario,salario = :salario,nomeFuncionario = :nomeFuncionario,senha = :senha, permissao = :permissao, rgFuncionario = :rgFuncionario, dataNascimento = :data WHERE idFuncionario = :idFuncionario");
            $param = array(
                ":cpfFuncionario" => $funcionario->getCpfFuncionario(),
                ":sexoFuncionario" => $funcionario->getSexoFuncionario(),
                ":salario" => $funcionario->getSalario(),
                ":nomeFuncionario" => $funcionario->getNomeFuncionario(),
                ":senha" => $funcionario->getSenha(),
                ":permissao" => $funcionario->getPermissao(),
                ":rgFuncionario" => $funcionario->getRgFuncionario(),
                ":data" => $funcionario->getDataNasc(),
                ":idFuncionario" => $idFuncionario,
            );
            $stmt->execute($param);

            $stmt2 = $this->pdo->prepare("UPDATE pessoa SET enderecoPessoa = :enderecoPessoa, bairroPessoa = :bairroPessoa,cidadePessoa = :cidadePessoa,cepPessoa =  :cepPessoa,emailPessoa = :emailPessoa,telefonePessoa = :telefonePessoa,ufPessoa = :ufPessoa WHERE :idFuncionario = idFuncionario");
            $param = array(
                ":enderecoPessoa" => $funcionario->getEndereco(),
                ":bairroPessoa" => $funcionario->getBairro(),
                ":cidadePessoa" => $funcionario->getCidade(),
                ":cepPessoa" => $funcionario->getCep(),
                ":emailPessoa" => $funcionario->getEmail(),
                ":telefonePessoa" => $funcionario->getTelefone(),
                ":ufPessoa" => $funcionario->getUf(),
                ":idFuncionario" => $idFuncionario,
            );

            $stmt2->execute($param);

            return true;
        }catch (PDOException $ex){
            echo "ERRO 02: {$ex->getMessage()}";
            return false;
        }
    }

    function login($emailFuncionario, $senha) {
        try {
        $stmt = $this->pdo->prepare("SELECT * FROM pessoa INNER JOIN funcionario ON pessoa.idFuncionario = funcionario.idFuncionario WHERE pessoa.emailPessoa = :emailFuncionario AND funcionario.senha = :senha ");
        $param = array(
                ":emailFuncionario" => $emailFuncionario,
                ":senha" => $senha,

        );

            $stmt->execute($param);
            $consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($stmt->rowCount() > 0) {
                foreach ($consulta as $row) {
                    $idFuncionario = $row['idFuncionario'];
                    $nomeFuncionario = $row['nomeFuncionario'];
                    $permissao = $row['permissao'];
                }
                session_start();
                $_SESSION['idFuncionario'] = $idFuncionario;
                $_SESSION['nomeFuncionario'] = $nomeFuncionario;
                $_SESSION['permissao'] = $permissao;

                echo $permissao;
                return true;
            } else {
                return false;
            }
        } catch (PDOException $ex) {
            echo "ERRO 04: {$ex->getMessage()}";
        }
    }

    function logout(){
        session_start();
        if(session_destroy()){
            ?>
             <script type="text/javascript">
            document.location.href = "../view/login.php";
        </script>
    <?php
        }

    }
     function filtrar($funcionario){
        $data = $_POST['filtro'];
        try{
            $stmt = $this->pdo->prepare("SELECT pessoa.enderecoPessoa, pessoa.bairroPessoa, pessoa.cidadePessoa, pessoa.cepPessoa, pessoa.emailPessoa, pessoa.telefonePessoa, pessoa.ufPessoa, funcionario.cpfFuncionario, funcionario.sexoFuncionario,funcionario.salario,funcionario.nomeFuncionario, funcionario.senha, funcionario.permissao, funcionario.rgFuncionario, funcionario.dataNascimento FROM pessoa INNER JOIN funcionario ON pessoa.idFuncionario = funcionario.idFuncionario WHERE month(dataPessoa) = ?");

            $stmt->bindValue( 1 , $data, PDO::PARAM_STR );
            $stmt->execute();

            $consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $consulta;

        }catch (PDOException $ex){
            echo "ERRO 02: {$ex->getMessage()}";
        }
    }
}
?>
