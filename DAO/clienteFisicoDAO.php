<?php

    require_once("conexao.php");

class clientefisicoDAO {

    //construtor
    function clientefisicoDAO(){
        $this->con = new Conexao();
        $this->pdo = $this->con->Connect();
    }

    function cadastrar($clienteFisico) {
        try {
            $stmt = $this->pdo->prepare("INSERT INTO clientefisico VALUES ('', :nome, :cpf, :rg, :sexo, :datanasc, :idade, :profissao, :estadocivil, 'A')");

            $param = array(
                ":nome" => $clienteFisico->getNomeCliente(),
                ":cpf" => $clienteFisico->getCpfCliente(),
                ":rg" => $clienteFisico->getRgCliente(),
                ":sexo" => $clienteFisico->getSexoCliente(),
                ":datanasc" => $clienteFisico->getDataNascimento(),
                ":idade" => $clienteFisico->getIdade(),
                ":profissao" => $clienteFisico->getProfissao(),
                ":estadocivil" => $clienteFisico->getEstadoCivil(),
            );

             $stmt->execute($param);

            $stmt = $this->pdo->prepare("INSERT INTO pessoa VALUES ('',LAST_INSERT_ID(),'',:endereco, :bairro, :cidade, :cep, :email, :telefone, :cnh, :uf,'', NOW())");
            $param = array(
                ":endereco" => $clienteFisico->getEndereco(),
                ":bairro" => $clienteFisico->getBairro(),
                ":cidade" => $clienteFisico->getCidade(),
                ":cep" => $clienteFisico->getCep(),
                ":email" => $clienteFisico->getEmail(),
                ":telefone" => $clienteFisico->getTelefone(),
                ":cnh" => $clienteFisico->getCnhPessoa(),
                ":uf" => $clienteFisico->getUf(),
            );

            $stmt->execute($param);

            return true;

        } catch (PDOException $ex) {
            echo "ERRO 01: {$ex->getMessage()}";

        }finally{
            // close connection;
        }
    }

       function consultarAll($pag) {
        $qtdlimite = 10;
        $apartirde = $pag*$qtdlimite;
        try{
            $stmt = $this->pdo->prepare("SELECT * FROM clientefisico WHERE status = 'A' ORDER BY idClienteFisico DESC LIMIT $apartirde, $qtdlimite ");

            $stmt->execute();
            $consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $consulta;

        } catch (PDOException $ex) {
            echo "ERRO 02: {$ex->getMessage()}";
        }
    }


     function numeroLinhas(){
        $limite = 10;
        $sql_total = "SELECT * FROM clientefisico WHERE status = 'A'";
        try{
            $query_Total = $this->pdo->prepare($sql_total);
            $query_Total->execute();

            $query_result = $query_Total->fetchAll(PDO::FETCH_ASSOC);
            $query_count =  $query_Total->rowCount(PDO::FETCH_ASSOC);

            return $query_count;

        } catch(PDOException $ex){
            echo "ERRO: {$ex->getMessage()}";
        }
    }

    function consultarbyId($id) {

        try{
            $stmt = $this->pdo->prepare("SELECT pessoa.enderecoPessoa, pessoa.bairroPessoa, pessoa.cidadePessoa, pessoa.cepPessoa, pessoa.emailPessoa, pessoa.telefonePessoa, pessoa.ufPessoa, pessoa.cnhPessoa, clientefisico.nomeCliente, clientefisico.cpfCliente, clientefisico.rgCliente, clientefisico.sexoCliente, clientefisico.dataNascimento, clientefisico.idade, clientefisico.profissao, clientefisico.profissao, clientefisico.estadocivil  FROM pessoa INNER JOIN clientefisico ON pessoa.idClienteFisico = clientefisico.idClienteFisico WHERE clientefisico.idClienteFisico = :id  ");

            $stmt->bindParam( ':id' , $id , PDO::PARAM_STR );

            $stmt->execute();
            $consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $consulta;

        } catch (PDOException $ex) {
            echo "ERRO 02: {$ex->getMessage()}";
        }
    }

    function Deletar($id){
        try{
            $stmt = $this->pdo->prepare("UPDATE clientefisico SET Status = 'I' WHERE idClienteFisico = :id ");

            $stmt->bindParam(':id' , $id , PDO::PARAM_STR );
            $stmt->execute();
            return true;


        }catch(PDOException $ex) {

            echo "ERRO 02: {$ex->getMessage()}";
            return false;
        }
    }

    function consultar($clientefisico) {
        $nome = $_POST['nome'];
        try{
            $stmt = $this->pdo->prepare("SELECT * FROM clientefisico WHERE nomeCliente LIKE ? ");

            $stmt->bindValue( 1 , "%$nome%", PDO::PARAM_STR );

            $stmt->execute();
            $consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $consulta;
        } catch (PDOException $ex) {
            echo "ERRO 02: {$ex->getMessage()}";
        }
    }

        function alterar($clienteFisico, $idCliente){
            try{
                $stmt = $this->pdo->prepare("UPDATE clientefisico SET nomeCliente = :nome, cpfCliente = :cpfCliente, rgCliente = :rgCliente, sexoCliente = :sexoCliente, dataNascimento = :dataNascimento, idade = :idade, profissao = :profissao, estadocivil = :estadocivil WHERE idClienteFisico = :idClienteFisico");
                 $param = array(
                ":nome" => $clienteFisico->getNomeCliente(),
                ":cpfCliente" => $clienteFisico->getCpfCliente(),
                ":rgCliente" => $clienteFisico->getRgCliente(),
                ":sexoCliente" => $clienteFisico->getSexoCliente(),
                ":dataNascimento" => $clienteFisico->getDataNascimento(),
                ":idade" => $clienteFisico->getIdade(),
                ":profissao" => $clienteFisico->getProfissao(),
                ":estadocivil" => $clienteFisico->getEstadoCivil(),
                ":idClienteFisico" => $idCliente,
            );
                 $stmt->execute($param);

                  $stmt2 = $this->pdo->prepare("UPDATE pessoa SET enderecoPessoa = :endereco, bairroPessoa = :bairro, cidadePessoa = :cidade, cepPessoa = :cep, emailPessoa = :email, telefonePessoa = :telefone, cnhPessoa = :cnh, ufPessoa = :uf WHERE idClienteFisico = :idClienteFisico");
                  $param = array(
                ":endereco" => $clienteFisico->getEndereco(),
                ":bairro" => $clienteFisico->getBairro(),
                ":cidade" => $clienteFisico->getCidade(),
                ":cep" => $clienteFisico->getCep(),
                ":email" => $clienteFisico->getEmail(),
                ":telefone" => $clienteFisico->getTelefone(),
                ":cnh" => $clienteFisico->getCnhPessoa(),
                ":uf" => $clienteFisico->getUf(),
                ":idClienteFisico" => $idCliente,
            );
                $stmt2->execute($param);

                return true;

            }catch (PDOException $ex) {
            echo "ERRO 02: {$ex->getMessage()}";
                return false;
        }
    }

    function filtrar($clienteFisico){
        $data = $_POST['filtro'];
        try{
            $stmt = $this->pdo->prepare("SELECT pessoa.enderecoPessoa, pessoa.bairroPessoa, pessoa.cidadePessoa, pessoa.cepPessoa, pessoa.emailPessoa, pessoa.telefonePessoa, pessoa.ufPessoa, pessoa.cnhPessoa, clientefisico.nomeCliente, clientefisico.cpfCliente, clientefisico.rgCliente, clientefisico.sexoCliente, clientefisico.dataNascimento, clientefisico.idade, clientefisico.profissao, clientefisico.profissao, clientefisico.estadocivil  FROM pessoa INNER JOIN clientefisico ON pessoa.idClienteFisico = clientefisico.idClienteFisico WHERE month(dataPessoa) = ?");

            $stmt->bindValue( 1 , $data, PDO::PARAM_STR );
            $stmt->execute();

            $consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $consulta;

        }catch (PDOException $ex){
            echo "ERRO 02: {$ex->getMessage()}";
        }
    }
}
?>
