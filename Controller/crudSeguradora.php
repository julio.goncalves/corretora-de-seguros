<link rel="stylesheet" type="text/css" href="../View/js/bootstrap-sweetalert-master/lib/sweet-alert.css">
<script type="text/javascript" src="../View/js/bootstrap-sweetalert-master/lib/sweet-alert.min.js"></script>
<?php  

	require_once ("../DAO/seguradoraDAO.php");
	require_once ("../Model/seguradora.php");

	$dao = new seguradoraDAO();
	$seguradora = new seguradora();
	
	if (isset($pagina)) {
	    $dados = $dao->consultarAll($pagina);
	    $qtdPag = $dao->numeroLinhas();
	}	 

	if (isset($_POST['btnSubmit'])) {

		$seguradora->setNomeSeguradora($_POST['nomeFantasia']);		
		$seguradora->setRazaoSocial($_POST['razaoSocial']);
		$seguradora->setRepresentante($_POST['representante']);
		$seguradora->setCnpj($_POST['cnpj']);
		$seguradora->setCepSeguradora($_POST['cep']);
		$seguradora->setLogradouraSeguradora($_POST['endereco']);
		$seguradora->setCidadeSeguradora($_POST['cidade']);
		$seguradora->setHomepage($_POST['website']);
		$seguradora->setBairroSeguradora($_POST['bairro']);
		$seguradora->setUfSeguradora($_POST['uf']);
		$seguradora->setTelefoneSeguradora($_POST['telefone']);
		$seguradora->setEmailSeguradora($_POST['email']);

		$retorno = $dao->cadastrar($seguradora);
		if($retorno){
            @session_start();
            $_SESSION['mensagem'] = 1;
        }
	}

	if (isset($_POST['btnBuscar'])){

		$seguradora->setNomeSeguradora($_POST['nomeSeguradora']);

		$dados = $dao->consultar($seguradora);
	}

	 if (isset($_GET['idSeguradora'])) {       
        $id = $_GET['idSeguradora'];
        $dados = $dao->consultarById($id);
        
        foreach ($dados as $row){
        
        }
    }

    //deletar seguradora
    if (isset($_GET['id'])) {
         $id = $_GET['id'];
         $retorno = $dao->deletar($id);
         if($retorno){
             @session_start();
            $_SESSION['mensagem'] = 1;
        }

        header("location: ../View/seguradora_listar.php?p=0");
    }

    if (isset($_POST['btnAlterar'])) {
    	$idSeguradora = $_POST['idSeguradora'];

    	$seguradora->setNomeSeguradora($_POST['nomeFantasia']);		
		$seguradora->setRazaoSocial($_POST['razaoSocial']);
		$seguradora->setRepresentante($_POST['representante']);
		$seguradora->setCnpj($_POST['cnpj']);
		$seguradora->setCepSeguradora($_POST['cep']);
		$seguradora->setLogradouraSeguradora($_POST['endereco']);
		$seguradora->setCidadeSeguradora($_POST['cidade']);
		$seguradora->setHomepage($_POST['website']);
		$seguradora->setBairroSeguradora($_POST['bairro']);
		$seguradora->setUfSeguradora($_POST['uf']);
		$seguradora->setTelefoneSeguradora($_POST['telefone']);
		$seguradora->setEmailSeguradora($_POST['email']);

		$retorno = $dao->alterar($seguradora, $idSeguradora);
		if($retorno){
            @session_start();
            $_SESSION['mensagem'] = 1;
            header('location: ../View/seguradora_alterar.php?idSeguradora='. $idSeguradora);
        }

    }

    if (isset($_POST['btFiltrar'])) {
        
       $seguradora = $_POST['filtro'];

       $dados =  $dao->filtrar($seguradora); 

       echo $seguradora;
    }

?>