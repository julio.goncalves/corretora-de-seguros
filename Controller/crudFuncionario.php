<link rel="stylesheet" type="text/css" href="../View/js/bootstrap-sweetalert-master/lib/sweet-alert.css">
<script type="text/javascript" src="../View/js/bootstrap-sweetalert-master/lib/sweet-alert.min.js"></script>
<?php

require_once("../DAO/funcionarioDAO.php");
require_once("../Model/funcionario.php");

    $dao = new funcionarioDAO();
    $funcionario = new funcionario();


if (isset($pagina)) {
    $dados = $dao->consultarAll($pagina);
    $qtdPag = $dao->numeroLinhas(); 
}       

if (isset($_POST['btnSubmit'])) {

    $funcionario->setNomeFuncionario($_POST['nome']);
    $funcionario->setCpfFuncionario($_POST['cpf']);
    $funcionario->setRgFuncionario($_POST['rg']);
    $funcionario->setDataNasc($_POST['dataNasc']);
    $funcionario->setCep($_POST['cep']);
    $funcionario->setEndereco($_POST['endereco']);
    $funcionario->setPermissao($_POST['permissao']);
    $funcionario->setSalario($_POST['salario']);
    $funcionario->setBairro($_POST['bairro']);
    $funcionario->setUf($_POST['uf']);
    $funcionario->setTelefone($_POST['telefone']);
    $funcionario->setEmail($_POST['email']);
    $funcionario->setSexoFuncionario($_POST['sexo']);
    $funcionario->setSenha($_POST['senha']);
    $funcionario->setCidade($_POST['cidade']);


    $retorno = $dao->cadastrar($funcionario);    

    if($retorno){
            @session_start();
            $_SESSION['mensagem'] = 1;
        } 
}

    //pesquisar
    if (isset($_POST['btnBuscar'])) {
        
        $funcionario->setNomeFuncionario($_POST['nome']);

        $dados = $dao->consultar($funcionario);
    }

    //deletar funcionario
    if (isset($_GET['id'])) {
         $id = $_GET['id'];
         $retorno = $dao->Deletar($id);
         if($retorno){
             @session_start();
            $_SESSION['mensagem'] = 2;
        }

        header("location: ../View/funcionario_listar.php?p=0");
    }

     if (isset($_GET['idFuncionario'])) {       
        $id = $_GET['idFuncionario'];
        $dados = $dao->consultarById($id);
        
        foreach ($dados as $row){
        
        }
    }

    //alterar
    if (isset($_POST['btnAlterar'])) {

        $idFuncionario = $_POST['idFuncionario'];

        $funcionario->setNomeFuncionario($_POST['nome']);
        $funcionario->setCpfFuncionario($_POST['cpf']);
        $funcionario->setRgFuncionario($_POST['rg']);
        $funcionario->setDataNasc($_POST['dataNasc']);
        $funcionario->setCep($_POST['cep']);
        $funcionario->setEndereco($_POST['endereco']);
        $funcionario->setPermissao($_POST['permissao']);
        $funcionario->setSalario($_POST['salario']);
        $funcionario->setBairro($_POST['bairro']);
        $funcionario->setUf($_POST['uf']);
        $funcionario->setTelefone($_POST['telefone']);
        $funcionario->setEmail($_POST['email']);
        $funcionario->setSexoFuncionario($_POST['sexo']);
        $funcionario->setSenha($_POST['senha']);
        $funcionario->setCidade($_POST['cidade']);

        $retorno = $dao->alterar($funcionario, $idFuncionario);

        if($retorno){
            @session_start();
            $_SESSION['mensagem'] = 1;
            header('location: ../View/funcionario_alterar.php?idFuncionario='. $idFuncionario);
        }
    }

     if (isset($_POST['btFiltrar'])) {
        
       $funcionario = $_POST['filtro'];

       $dados =  $dao->filtrar($funcionario);
    }

  
?>
