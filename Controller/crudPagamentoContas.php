<?php  
	require_once ("../DAO/pagamentoContasDAO.php");
	require_once ("../Model/pagamentoContas.php");

	$dao =  new pagamentoContasDAO();
    $pagamentoContas = new pagamentoContas();

    $dados = $dao->consultarAll();

	$consulta = $dao->soma();	

    $sum = 0;
        foreach($consulta as $key=>$value){
            $sum+= $value['valorPagamento'];
        }

    if (isset($_POST['btnPagar'])) {

    	$pagamentoContas->setValorTotal($sum);
    	$dao->cadastrarSoma($pagamentoContas);
    	
    }

    if (isset($_POST['btnCadastrar'])) {       

        $pagamentoContas->setDescricao($_POST['descricao']);
        $pagamentoContas->setValor($_POST['valor']);
        
        $dao->cadastrar($pagamentoContas);
        header("Refresh:0");
    }

    
    if (isset($_POST['btFiltrar'])) {
        
        $pagamentoContas->setDataPagamento($_POST['filtro']);

       $busca =  $dao->filtrar($pagamentoContas); 

       $sum = 0;
        foreach($busca as $key=>$value){
            $sum+= $value['valorPagamento'];
        }  

    }
    
?>