<link rel="stylesheet" type="text/css" href="../View/js/bootstrap-sweetalert-master/lib/sweet-alert.css">
<script type="text/javascript" src="../View/js/bootstrap-sweetalert-master/lib/sweet-alert.min.js"></script>
<?php 
    require_once("../DAO/clienteFisicoDAO.php");
    require_once("../Model/clienteFisico.php");

    
    $dao = new clienteFisicoDAO();

    if (isset($pagina)){
        $dados = $dao->consultarAll($pagina);
        $qtdPag = $dao->numeroLinhas(); 
    }       

    if(isset($_POST['Cadastrar'])){
        $clienteFisico = new clienteFisico();
        
        $clienteFisico->setNomeCliente($_POST['nome']);
        $clienteFisico->setCpfCliente($_POST['cpf']);
        $clienteFisico->setRgCliente($_POST['rg']);
        $clienteFisico->setSexoCliente($_POST['sexo']);
        $clienteFisico->setDataNascimento($_POST['datanasc']);
        $clienteFisico->setIdade($_POST['idade']);
        $clienteFisico->setProfissao($_POST['profissao']);
        $clienteFisico->setEstadoCivil($_POST['estadocivil']);
        $clienteFisico->setCep($_POST['cep']);
        $clienteFisico->setEndereco($_POST['endereco']);
        $clienteFisico->setBairro($_POST['bairro']);
        $clienteFisico->setCnhPessoa($_POST['cnh']);
        $clienteFisico->setUf($_POST['uf']);
        $clienteFisico->setTelefone($_POST['telefone']);
        $clienteFisico->setEmail($_POST['email']);
        $clienteFisico->setCidade($_POST['cidade']);
        
        $dao = new clienteFisicoDAO();
        $retorno = $dao->cadastrar($clienteFisico);

        if($retorno){
            @session_start();
            $_SESSION['mensagem'] = 1;
        }
        header("location: ../View/clienteFisico_listar.php?p=0");    
    }

    // Deletar Cliente
    if(isset($_GET['id'])){
        $id = $_GET['id'];
        $dao = new clienteFisicoDAO();
        $retorno = $dao->Deletar($id);
        if($retorno){
             @session_start();
            $_SESSION['mensagem'] = 1;
        }
        header("location: ../View/clienteFisico_listar.php?p=0");
    }

    // Consultar Cliente
    if (isset($_GET['idCliente'])) {       
        $id = $_GET['idCliente'];
        
        $dao = new clienteFisicoDAO();
        $dados = $dao->consultarById($id);
        
        foreach ($dados as $row){
        
        }
    }

    if (isset($_POST['btnBuscar'])) {
        $clienteFisico = new clienteFisico();

        $clienteFisico->setNomeCliente($_POST['nome']);
        $dados = $dao->consultar($clienteFisico);  
    }

    if (isset($_POST['btnAlterar'])) {
        $clienteFisico = new clienteFisico();
        
        $idCliente = $_POST['idClienteFisico'];
        
        $clienteFisico->setNomeCliente($_POST['nome']);
        $clienteFisico->setCpfCliente($_POST['cpf']);
        $clienteFisico->setRgCliente($_POST['rg']);
        $clienteFisico->setSexoCliente($_POST['sexo']);
        $clienteFisico->setDataNascimento($_POST['datanasc']);
        $clienteFisico->setIdade($_POST['idade']);
        $clienteFisico->setProfissao($_POST['profissao']);
        $clienteFisico->setEstadoCivil($_POST['estadocivil']);
        $clienteFisico->setCep($_POST['cep']);
        $clienteFisico->setEndereco($_POST['endereco']);
        $clienteFisico->setBairro($_POST['bairro']);
        $clienteFisico->setCnhPessoa($_POST['cnh']);
        $clienteFisico->setUf($_POST['uf']);
        $clienteFisico->setTelefone($_POST['telefone']);
        $clienteFisico->setEmail($_POST['email']);
        $clienteFisico->setCidade($_POST['cidade']);
        
        $dao = new clienteFisicoDAO();
        
        $retorno = $dao->alterar($clienteFisico, $idCliente);
        
        if($retorno){
            @session_start();
            $_SESSION['mensagem'] = 1;
            header('location: ../View/clienteFisico_alterar.php?idCliente='. $idCliente);
        }

    }

    if (isset($_POST['btFiltrar'])) {
        
       $clienteFisico = $_POST['filtro'];

       $dados =  $dao->filtrar($clienteFisico);
    }

?>