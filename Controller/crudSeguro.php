<link rel="stylesheet" type="text/css" href="../View/js/bootstrap-sweetalert-master/lib/sweet-alert.css">
<script type="text/javascript" src="../View/js/bootstrap-sweetalert-master/lib/sweet-alert.min.js"></script>

<?php
        require_once("../DAO/seguroDAO.php");
        require_once("../Model/clienteFisico.php");
        require_once("../Model/veiculo.php");

                $dao = new seguroDao();
                $clienteFisico = new clienteFisico();
                $veiculo = new seguroVeiculo();

        if(isset($_POST['Cadastrar'])){

                $idFuncionario = $_POST['idFuncionario'];
                $clienteFisico->setNomeCliente($_POST['nome']);
                $clienteFisico->setCpfCliente($_POST['cpf']);
                $clienteFisico->setRgCliente($_POST['rg']);
                $clienteFisico->setSexoCliente($_POST['sexo']);
                $clienteFisico->setDataNascimento($_POST['datanasc']);
                $clienteFisico->setIdade($_POST['idade']);
                $clienteFisico->setProfissao($_POST['profissao']);
                $clienteFisico->setEstadoCivil($_POST['estadocivil']);
                $clienteFisico->setCep($_POST['cep']);
                $clienteFisico->setEndereco($_POST['endereco']);
                $clienteFisico->setBairro($_POST['bairro']);
                $clienteFisico->setCnhPessoa($_POST['cnh']);
                $clienteFisico->setUf($_POST['uf']);
                $clienteFisico->setTelefone($_POST['telefone']);
                $clienteFisico->setEmail($_POST['email']);
                $clienteFisico->setCidade($_POST['cidade']);

                $veiculo->setMarca($_POST['fabricanteCarro']);
                $veiculo->setModelo($_POST['modeloCarro']);
                $veiculo->setCombustivelCarro($_POST['combustivelCarro']);
                $veiculo->setAno($_POST['anoCarro']);
                $veiculo->setPlaca($_POST['placaCarro']);
                $veiculo->setNumeroPortas($_POST['nPortasCarro']);
                $veiculo->setUsoVeiculo($_POST['usoCarro']);
                $veiculo->setGaragem($_POST['garagemCarro']);
                $veiculo->setSeguranca($_POST['seguranca']);
                $veiculo->setpossuiPortao($_POST['possuiPortao']);
                //$veiculo->setTipoPagamento($_POST['tipoPagamento']);
                //$veiculo->setValor($_POST['valor']);
                $veiculo->setData($_POST['data']);
                
                $imagem = $_FILES["image"];

                if($imagem != NULL) { 
                        $nomeFinal = time().'.jpg';
                    
                        $caminho_imagem = "../Controller/img/" . $nomeFinal;
                    
                        if (move_uploaded_file($imagem['tmp_name'], $caminho_imagem)) {

                                $restorno = $dao->cadastrarSeguro($clienteFisico, $veiculo, $idFuncionario, $nomeFinal);

                                if($restorno){

                                        @session_start();
                                        $mensagem = $_SESSION['mensagem'] = 1;
                                }
                        }
                } 
        }


?>