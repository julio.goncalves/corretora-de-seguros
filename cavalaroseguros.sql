-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 06-Jun-2016 às 23:10
-- Versão do servidor: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cavalaroseguros`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientefisico`
--

CREATE TABLE `clientefisico` (
  `idClienteFisico` int(11) NOT NULL,
  `nomeCliente` varchar(70) NOT NULL,
  `cpfCliente` varchar(14) NOT NULL,
  `rgCliente` varchar(12) NOT NULL,
  `sexoCliente` varchar(1) NOT NULL,
  `dataNascimento` date NOT NULL,
  `idade` int(11) NOT NULL,
  `profissao` varchar(70) NOT NULL,
  `estadocivil` varchar(20) NOT NULL,
  `status` varchar(1) NOT NULL,
  `image` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `clientefisico`
--

INSERT INTO `clientefisico` (`idClienteFisico`, `nomeCliente`, `cpfCliente`, `rgCliente`, `sexoCliente`, `dataNascimento`, `idade`, `profissao`, `estadocivil`, `status`, `image`) VALUES
(1, 'Henry Ordine', '418.125.248-54', '49.718.699-8', 'M', '1111-11-11', 25, 'Programador', 'Solteiro ', 'A', NULL),
(2, 'Luan de Moraes', '418.125.248-54', '49.718.699-8', 'M', '1993-09-26', 22, 'Vagal', 'Solteiro ', 'A', NULL),
(3, 'Henry Ordine', '418.125.248-54', '49.718.699-8', 'F', '1993-09-26', 23, 'Vagal', 'Casado', 'A', NULL),
(4, 'Rafael Ferreira', '418.125.248-54', '49.718.699-8', 'M', '1990-01-01', 25, 'Web Designer', 'Casado', 'A', NULL),
(5, 'Ana Julia Gomes', '418.125.248-54', '49.718.699-8', 'F', '1993-09-26', 20, 'Jornalista', 'Solteiro ', 'A', '1464575079.jpg'),
(6, 'Rafael Ferreira', '418.125.248-54', '49.718.699-8', 'M', '1993-09-26', 14, 'Vagal', 'Solteiro ', 'A', '1464645095.jpg'),
(7, 'Rafael Ferreira', '418.125.248-54', '49.718.699-8', 'M', '1993-09-26', 14, 'Vagal', 'Solteiro ', 'A', '1464645130.jpg'),
(8, 'Filipe Aguiar', '418.125.248-54', '49.718.699-8', 'M', '2014-10-01', 10, 'Programador', 'Casado', 'A', '1464645218.jpg'),
(9, 'Mariana', '418.125.248-54', '49.718.699-8', 'F', '1990-03-05', 7, 'Vagal', 'Casado', 'A', '1464645422.jpg'),
(10, 'Maria José', '418.125.248-54', '49.718.699-8', 'M', '1991-11-30', 13, 'Vagal', 'Solteiro ', 'A', '1464646176.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientejuridico`
--

CREATE TABLE `clientejuridico` (
  `idClienteJuridico` int(11) NOT NULL,
  `razaoSocial` varchar(50) NOT NULL,
  `cnpj` varchar(18) NOT NULL,
  `inscricaoEstadual` varchar(15) NOT NULL,
  `nomeFantasia` varchar(50) NOT NULL,
  `nomeRepresentante` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `comissao`
--

CREATE TABLE `comissao` (
  `idComissao` int(11) NOT NULL,
  `valorCom` float NOT NULL,
  `dataComissao` date NOT NULL,
  `idSeguroCarro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `comissao`
--

INSERT INTO `comissao` (`idComissao`, `valorCom`, `dataComissao`, `idSeguroCarro`) VALUES
(1, 87.5, '2016-05-21', 1),
(2, 87.5, '2016-05-23', 2),
(4, 75, '2016-05-28', 3),
(5, 75, '2016-05-28', 3),
(6, 75.0265, '2016-05-29', 4),
(7, 75, '2016-05-30', 8),
(8, 25, '2016-05-30', 7),
(9, 0.5, '2016-05-30', 9),
(10, 5, '2016-05-30', 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

CREATE TABLE `funcionario` (
  `idFuncionario` int(11) NOT NULL,
  `cpfFuncionario` varchar(14) NOT NULL,
  `sexoFuncionario` char(1) NOT NULL,
  `salario` float NOT NULL,
  `nomeFuncionario` varchar(100) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `permissao` varchar(50) NOT NULL,
  `rgFuncionario` varchar(20) NOT NULL,
  `idGerente` int(11) DEFAULT NULL,
  `idFuncionarioCorretor` int(11) DEFAULT NULL,
  `idFuncionarioFinanceiro` int(11) DEFAULT NULL,
  `status` varchar(1) NOT NULL,
  `dataNascimento` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `funcionario`
--

INSERT INTO `funcionario` (`idFuncionario`, `cpfFuncionario`, `sexoFuncionario`, `salario`, `nomeFuncionario`, `senha`, `permissao`, `rgFuncionario`, `idGerente`, `idFuncionarioCorretor`, `idFuncionarioFinanceiro`, `status`, `dataNascimento`) VALUES
(1, '418.125.248-54', 'm', 1500, 'Julio Cesar Gonçalves Rodrigues', '123', 'financeiro', '49.718.699-8', 0, 0, 0, 'A', '1993-09-26'),
(2, '418.125.248-54', 'm', 1200, 'Henry Ordine', '123', 'financeiro', '49.718.699-8', 0, 0, 0, 'A', '1993-09-26'),
(3, '418.125.248-54', 'm', 1200, 'Filipe Aguiar', '123', 'financeiro', '49.718.699-8', 0, 0, 0, 'A', '1993-09-26'),
(4, '418.125.248-54', 'm', 1250, 'Luan de Moraes', '123', 'corretor', '49.718.699-8', 0, 0, 0, 'A', '1993-09-26'),
(5, '418.125.248-54', 'm', 10500, 'Edson Arantes do Nascimento', '123', 'administrador', '49.718.699-8', 0, 0, 0, 'A', '1993-09-26');

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionariocorretor`
--

CREATE TABLE `funcionariocorretor` (
  `idFuncionarioCorretor` int(11) NOT NULL,
  `comissao` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionariofinanceiro`
--

CREATE TABLE `funcionariofinanceiro` (
  `idFuncionarioFinanceiro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `gerente`
--

CREATE TABLE `gerente` (
  `idGerenteq` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pagamentocontas`
--

CREATE TABLE `pagamentocontas` (
  `idPagamento` int(11) NOT NULL,
  `descricaoPagamento` varchar(100) DEFAULT NULL,
  `valorPagamento` float DEFAULT NULL,
  `valorTotal` float DEFAULT NULL,
  `dataPagamento` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pagamentocontas`
--

INSERT INTO `pagamentocontas` (`idPagamento`, `descricaoPagamento`, `valorPagamento`, `valorTotal`, `dataPagamento`) VALUES
(1, 'Comissão dos Fucionarios', 9800, NULL, '2016-05-21'),
(2, 'Extras', 132.25, NULL, '2016-05-27');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pagamentocontrato`
--

CREATE TABLE `pagamentocontrato` (
  `idPagamentoContrato` int(11) NOT NULL,
  `idSeguro` int(11) NOT NULL,
  `tipoPagamento` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa`
--

CREATE TABLE `pessoa` (
  `idPessoa` int(11) NOT NULL,
  `idClienteFisico` int(11) DEFAULT NULL,
  `idClienteJuridico` int(11) DEFAULT NULL,
  `enderecoPessoa` varchar(100) NOT NULL,
  `bairroPessoa` varchar(100) NOT NULL,
  `cidadePessoa` varchar(100) NOT NULL,
  `cepPessoa` varchar(10) NOT NULL,
  `emailPessoa` varchar(100) NOT NULL,
  `telefonePessoa` varchar(14) NOT NULL,
  `cnhPessoa` varchar(20) NOT NULL,
  `ufPessoa` varchar(2) NOT NULL,
  `idFuncionario` int(11) NOT NULL,
  `dataPessoa` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pessoa`
--

INSERT INTO `pessoa` (`idPessoa`, `idClienteFisico`, `idClienteJuridico`, `enderecoPessoa`, `bairroPessoa`, `cidadePessoa`, `cepPessoa`, `emailPessoa`, `telefonePessoa`, `cnhPessoa`, `ufPessoa`, `idFuncionario`, `dataPessoa`) VALUES
(1, 0, 0, 'Rodovia Dom Pedro I, Km 103', 'Engenho D Agua', 'Itatiba', '13.252-800', 'jjulio_jc@hotmail.com', '1145383068', '0', 'SP', 1, '2016-05-21'),
(2, 1, 0, 'Rua Centro', 'Centro', 'Itatiba', '13252-492', 'henry@email.com', '4538-3068', '1515151', 'SP', 0, '2016-05-21'),
(3, 2, 0, 'Rua Rufino Pereira de Araujo', 'Engenho D Agua', 'Itatiba', '13252-492', 'floragestich@hotmail.com', '4538-3068', '999999999999', 'SP', 0, '2016-05-21'),
(4, 3, 0, 'Rua Rufino Pereira de Araujo', 'Engenho D Agua', 'Itatiba', '13252-492', 'aline@email.com', '4538-3068', '111', 'SP', 0, '2016-05-23'),
(5, 0, 0, 'Rua Rufino Pereira de Araujo', 'Engenho D Agua', 'Itatiba', '13.252-492', 'aline@email.com', '1145388564', '0', 'SP', 2, '2016-05-23'),
(6, 0, 0, 'Rua Rufino Pereira de G', 'Engenho D Agua', 'Jundiai', '13.252-493', 'filipe@email.com', '1145388568', '0', 'RJ', 3, '2016-05-23'),
(7, 0, 0, 'Rua Rufino Pereira de Araujo, 13', 'Engenho D Agua', 'Itatiba', '13.252-492', 'luan@email.com', '1145388564', '0', 'SP', 4, '2016-05-28'),
(8, 4, 0, 'Rua Rufino Pereira de Araujo, 13', 'Santa Cruz', 'Itatiba', '13252-492', 'rafael@email.com', '4538-3068', '121212', 'SP', 0, '2016-05-28'),
(9, 0, 0, 'Rodovia Dom Pedro I, Km 103', 'Engenho D Agua', 'Itatiba', '13.252-492', 'edson@email.com', '1145383068', '0', 'RJ', 5, '2016-05-28'),
(10, 5, 0, 'Rodovia Dom Pedro I, Km 103', 'Centro', 'Itatiba', '13.252-800', 'anajulia@hotmail.com', '1145383068', '2222222222', 'SP', 0, '2016-05-29'),
(11, 6, 0, 'Rua Rufino Pereira de Araujo, 13', 'Engenho D Agua', 'Itatiba', '13.252-492', 'aline@email.com', '1145388564', '111', 'SP', 0, '2016-05-30'),
(12, 7, 0, 'Rua Rufino Pereira de Araujo, 13', 'Engenho D Agua', 'Itatiba', '13.252-492', 'aline@email.com', '1145388564', '111', 'SP', 0, '2016-05-30'),
(13, 8, 0, 'Rua Rufino Pereira de G', 'Centro', 'Jundiai', '13.252-493', 'julio@email.com', '1145388568', '111', 'RJ', 0, '2016-05-30'),
(14, 9, 0, 'Rodovia Dom Pedro I, Km 103', 'Engenho D Agua', 'Itatiba', '13.252-800', 'mariana@hotmail.com', '4538-3068', '111', 'SP', 0, '2016-05-30'),
(15, 10, 0, 'Rodovia Dom Pedro I, Km 103', 'Engenho D Agua', 'Itatiba', '13252-800', 'maria@hotmail.com', '4538-3068', '111', 'SP', 0, '2016-05-30');

-- --------------------------------------------------------

--
-- Estrutura da tabela `seguradora`
--

CREATE TABLE `seguradora` (
  `idSeguradora` int(11) NOT NULL,
  `nomeSeguradora` varchar(100) NOT NULL,
  `cnpj` varchar(18) NOT NULL,
  `razaoSocial` varchar(50) NOT NULL,
  `telefoneSeguradora` varchar(20) NOT NULL,
  `homepage` varchar(100) NOT NULL,
  `representante` varchar(100) NOT NULL,
  `emailSeguradora` varchar(80) NOT NULL,
  `logradouroSeguradora` varchar(100) NOT NULL,
  `bairroSeguradora` varchar(80) NOT NULL,
  `cidadeSeguradora` varchar(80) NOT NULL,
  `cepSeguradora` varchar(10) NOT NULL,
  `ufSeguradora` varchar(2) NOT NULL,
  `status` varchar(1) NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `seguradora`
--

INSERT INTO `seguradora` (`idSeguradora`, `nomeSeguradora`, `cnpj`, `razaoSocial`, `telefoneSeguradora`, `homepage`, `representante`, `emailSeguradora`, `logradouroSeguradora`, `bairroSeguradora`, `cidadeSeguradora`, `cepSeguradora`, `ufSeguradora`, `status`, `data`) VALUES
(1, 'Ordine Seguro Representações Ltda.', '49.718.652/0001-23', 'Ordine Seguros', '1145388564', 'www.ordine.com.br', 'Henry Ordine', 'ordine@seguro.com', 'Rua Centro', 'Centro', 'Itatiba', '13.252-492', 'SP', 'I', '2016-05-21'),
(2, 'Ordine Seguros Ltda.', '49.718.652/0001-23', 'Ordine Seguros', '1145388564', 'www.ordine.com.br', 'Henry Ordine', 'aline@email.com', 'Rua Rufino Pereira de Araujo', 'Centro', 'Itatiba', '13.252-492', 'SP', 'A', '2016-05-27');

-- --------------------------------------------------------

--
-- Estrutura da tabela `seguro`
--

CREATE TABLE `seguro` (
  `idSeguro` int(11) NOT NULL,
  `idSimulacao` int(11) NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `seguroimovel`
--

CREATE TABLE `seguroimovel` (
  `idSeguroImovel` int(11) NOT NULL,
  `logradouroImovel` varchar(100) NOT NULL,
  `cepImovel` varchar(10) NOT NULL,
  `bairroImovel` varchar(80) NOT NULL,
  `cidadeImovel` varchar(80) NOT NULL,
  `ufImovel` varchar(2) NOT NULL,
  `qtdComodo` int(11) NOT NULL,
  `garagem` int(11) NOT NULL,
  `valorIMovel` float NOT NULL,
  `tipoImovel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `seguropessoa`
--

CREATE TABLE `seguropessoa` (
  `idSeguroPessoa` int(11) NOT NULL,
  `rendaMensal` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `seguroveiculo`
--

CREATE TABLE `seguroveiculo` (
  `idSeguroCarro` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL,
  `marca` varchar(50) NOT NULL,
  `modelo` varchar(100) NOT NULL,
  `combustivel` varchar(50) DEFAULT NULL,
  `ano` int(11) NOT NULL,
  `placa` varchar(8) NOT NULL,
  `numeroPortas` int(11) NOT NULL,
  `usoVeiculo` varchar(50) NOT NULL,
  `garagem` varchar(20) NOT NULL,
  `seguranca` varchar(20) NOT NULL,
  `possuiPortao` varchar(50) NOT NULL,
  `tipoPagamento` varchar(50) DEFAULT NULL,
  `valor` double DEFAULT NULL,
  `dataSeguro` date NOT NULL,
  `status` varchar(5) NOT NULL,
  `idFuncionario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `seguroveiculo`
--

INSERT INTO `seguroveiculo` (`idSeguroCarro`, `idCliente`, `marca`, `modelo`, `combustivel`, `ano`, `placa`, `numeroPortas`, `usoVeiculo`, `garagem`, `seguranca`, `possuiPortao`, `tipoPagamento`, `valor`, `dataSeguro`, `status`, `idFuncionario`) VALUES
(1, 2, 'Ford', 'Ka', 'Alcool', 2015, 'CAA-4214', 4, 'Faculdade', 'Residencia', 'nao', 'sim', 'dinheiro', 1750, '2016-05-21', 'A', 1),
(2, 3, 'Ford', 'Ka', 'Alcool', 2015, 'CAA-4214', 4, 'Faculdade', 'Trabalho', 'sim', 'nao', 'dinheiro', 5000.37, '2016-05-23', 'A', 1),
(3, 4, 'Citroen', 'C3', 'Gasolina/Alcool', 2016, 'CAA-4214', 4, 'Particular/Trabalho', 'Residencia', 'nao', 'sim', 'dinheiro', 1500, '2016-05-28', 'A', 4),
(4, 5, 'Ferrari', 'Enzo', 'Alcool', 2015, 'CAA-4214', 4, 'Faculdade', 'Residencia', 'sim', 'sim', 'dinheiro', 1500.53, '2016-05-29', 'A', 5),
(5, 6, 'Ford', 'BMW', 'Alcool', 2015, 'CAA-4214', 3, 'Trabalho', 'Trabalho', 'sim', 'sim', 'em aberto', 0, '2016-05-30', 'I', 5),
(6, 7, 'Ford', 'BMW', 'Alcool', 2015, 'CAA-4214', 3, 'Trabalho', 'Trabalho', 'sim', 'sim', 'dinheiro', 100, '2016-05-30', 'A', 5),
(7, 8, 'Ford', 'BMW', 'Gasolina', 2015, 'CAA-4214', 3, 'Faculdade', 'Trabalho', 'sim', 'sim', 'dinheiro', 500, '2016-05-30', 'A', 5),
(8, 9, 'Ford', 'BMW', 'Alcool', 2015, 'CAA-4214', 3, 'Trabalho', 'Trabalho', 'sim', 'sim', 'dinheiro', 1500, '2016-05-30', 'A', 5),
(9, 10, 'Ford', 'BMW', 'Alcool', 2015, 'CAA-4214', 2, 'Trabalho', 'Residencia', 'sim', 'sim', 'dinheiro', 10, '2016-05-30', 'A', 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `simulacao`
--

CREATE TABLE `simulacao` (
  `idSimulacao` int(11) NOT NULL,
  `idPessoa` int(11) NOT NULL,
  `idSeguradora` int(11) NOT NULL,
  `idFuncionarioCorretor` int(11) NOT NULL,
  `idSeguroVeiculo` int(11) NOT NULL,
  `idSeguroPessoa` int(11) NOT NULL,
  `idSeguroImovel` int(11) NOT NULL,
  `formaPagamento` varchar(50) NOT NULL,
  `valorSimulacao` float NOT NULL,
  `dataInicial` date NOT NULL,
  `dataFinal` date NOT NULL,
  `lembrete` varchar(100) NOT NULL,
  `copiaDocumentos` varchar(100) NOT NULL,
  `idSeguro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientefisico`
--
ALTER TABLE `clientefisico`
  ADD PRIMARY KEY (`idClienteFisico`);

--
-- Indexes for table `clientejuridico`
--
ALTER TABLE `clientejuridico`
  ADD PRIMARY KEY (`idClienteJuridico`);

--
-- Indexes for table `comissao`
--
ALTER TABLE `comissao`
  ADD PRIMARY KEY (`idComissao`);

--
-- Indexes for table `funcionario`
--
ALTER TABLE `funcionario`
  ADD PRIMARY KEY (`idFuncionario`);

--
-- Indexes for table `funcionariocorretor`
--
ALTER TABLE `funcionariocorretor`
  ADD PRIMARY KEY (`idFuncionarioCorretor`);

--
-- Indexes for table `funcionariofinanceiro`
--
ALTER TABLE `funcionariofinanceiro`
  ADD PRIMARY KEY (`idFuncionarioFinanceiro`);

--
-- Indexes for table `gerente`
--
ALTER TABLE `gerente`
  ADD PRIMARY KEY (`idGerenteq`);

--
-- Indexes for table `pagamentocontas`
--
ALTER TABLE `pagamentocontas`
  ADD PRIMARY KEY (`idPagamento`);

--
-- Indexes for table `pagamentocontrato`
--
ALTER TABLE `pagamentocontrato`
  ADD PRIMARY KEY (`idPagamentoContrato`);

--
-- Indexes for table `pessoa`
--
ALTER TABLE `pessoa`
  ADD PRIMARY KEY (`idPessoa`),
  ADD KEY `idClienteFisico` (`idClienteFisico`),
  ADD KEY `idClienteJuridico` (`idClienteJuridico`);

--
-- Indexes for table `seguradora`
--
ALTER TABLE `seguradora`
  ADD PRIMARY KEY (`idSeguradora`);

--
-- Indexes for table `seguro`
--
ALTER TABLE `seguro`
  ADD PRIMARY KEY (`idSeguro`);

--
-- Indexes for table `seguroimovel`
--
ALTER TABLE `seguroimovel`
  ADD PRIMARY KEY (`idSeguroImovel`);

--
-- Indexes for table `seguropessoa`
--
ALTER TABLE `seguropessoa`
  ADD PRIMARY KEY (`idSeguroPessoa`);

--
-- Indexes for table `seguroveiculo`
--
ALTER TABLE `seguroveiculo`
  ADD PRIMARY KEY (`idSeguroCarro`);

--
-- Indexes for table `simulacao`
--
ALTER TABLE `simulacao`
  ADD PRIMARY KEY (`idSimulacao`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientefisico`
--
ALTER TABLE `clientefisico`
  MODIFY `idClienteFisico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `clientejuridico`
--
ALTER TABLE `clientejuridico`
  MODIFY `idClienteJuridico` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comissao`
--
ALTER TABLE `comissao`
  MODIFY `idComissao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `funcionario`
--
ALTER TABLE `funcionario`
  MODIFY `idFuncionario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `funcionariocorretor`
--
ALTER TABLE `funcionariocorretor`
  MODIFY `idFuncionarioCorretor` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `funcionariofinanceiro`
--
ALTER TABLE `funcionariofinanceiro`
  MODIFY `idFuncionarioFinanceiro` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gerente`
--
ALTER TABLE `gerente`
  MODIFY `idGerenteq` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pagamentocontas`
--
ALTER TABLE `pagamentocontas`
  MODIFY `idPagamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pagamentocontrato`
--
ALTER TABLE `pagamentocontrato`
  MODIFY `idPagamentoContrato` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pessoa`
--
ALTER TABLE `pessoa`
  MODIFY `idPessoa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `seguradora`
--
ALTER TABLE `seguradora`
  MODIFY `idSeguradora` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `seguro`
--
ALTER TABLE `seguro`
  MODIFY `idSeguro` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `seguroimovel`
--
ALTER TABLE `seguroimovel`
  MODIFY `idSeguroImovel` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `seguropessoa`
--
ALTER TABLE `seguropessoa`
  MODIFY `idSeguroPessoa` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `seguroveiculo`
--
ALTER TABLE `seguroveiculo`
  MODIFY `idSeguroCarro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `simulacao`
--
ALTER TABLE `simulacao`
  MODIFY `idSimulacao` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
